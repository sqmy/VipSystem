import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import moment from 'moment'
import httpRequest from '@/utils/httpRequest'

/**
 * 获取uuid
 */
export function getUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
  })
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth(key) {
  return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate(data, id = 'id', pid = 'parentId') {
  var res = []
  var temp = {}
  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }
  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]]['children']) {
        temp[data[k][pid]]['children'] = []
      }
      if (!temp[data[k][pid]]['_level']) {
        temp[data[k][pid]]['_level'] = 1
      }
      data[k]['_level'] = temp[data[k][pid]]._level + 1
      temp[data[k][pid]]['children'].push(data[k])
    } else {
      res.push(data[k])
    }
  }
  return res
}

/**
 * 清除登录信息
 */
export function clearLoginInfo() {
  Vue.cookie.delete('token')
  store.commit('resetStore')
  router.options.isAddDynamicMenuRoutes = false
}

/**
 * 日期格式化
 * @param {number} time 日期
 * @param {string} formatStr 格式
 */
export function dateFormat(time, formatStr) {
  if (time == undefined) {
    return "";
  }
  formatStr = formatStr || "YYYY-MM-DD HH:mm:ss";
  return moment(time).format(formatStr)
}

/**
 * 获取当前登录账号能拿到的公司
 */
export function getCompanys(callback) {
  var companys = []
  httpRequest({
    url: httpRequest.adornUrl('/merchant/company/getByUser'),
    method: 'get'
  }).then(({
    data
  }) => {
    if (data && data.code === 0) {
      companys = data.data;
    }
    callback(companys)
  })
}

/**
 * 获取当前登录账号能拿到的公司
 */
export function getShopsByCompany(companyId, callback) {
  var shops = []
  if (companyId == null || companyId == undefined) {
    callback(shops)
  }
  httpRequest({
    url: httpRequest.adornUrl('/merchant/shop/getByCompany?companyId=' + companyId),
    method: 'get'
  }).then(({
    data
  }) => {
    if (data && data.code === 0) {
      shops = data.data;
    }
    callback(shops)
  })
}

/**
 * 获取当前登录账号能拿到的公司
 */
export function getKaidanUsers(companyId,callback) {
  var list = []
  httpRequest({
    url: httpRequest.adornUrl(`/merchant/kaidan-user/getByCompany?companyId=${companyId}`),
    method: 'get'
  }).then(({
    data
  }) => {
    if (data && data.code === 0) {
      list = data.data;
    }
    callback(list)
  })
}
