cd vip_admin_ui
npm install -g cnpm --registry=https://registry.npm.taobao.org
cnpm install
npm run build
cd ../vip_service
rm -rf src/main/resources/dist/
cp -rf ../vip_admin_ui/dist src/main/resources/dist
mvn -X clean package
rm -f /opt/docker/app/vipsystem/vipsystem.jar
cp target/vipsystem.jar /opt/docker/app/vipsystem
conid=`docker  ps -a| grep vipsystem | awk '{print $1}'`
if [ -n "$conid" ]; then
docker stop $conid
echo "stop 容器id:" $conid
docker restart $conid
else
echo "容器不存在,创建容器"
cd /opt/docker/app/vipsystem
docker-compose up -d
fi