SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '系统验证码';

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COMMENT = '系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 0, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '系统日志';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态:1-正常,0-禁用',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COMMENT = '菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0, 1);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1, 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2, 1);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3, 0);
INSERT INTO `sys_menu` VALUES (5, 1, 'SQL监控', 'http://localhost:8080/renren-fast/druid/sql.html', NULL, 1, 'sql', 4, 0);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6, 0);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7, 0);
INSERT INTO `sys_menu` VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'oss', 6, 0);
INSERT INTO `sys_menu` VALUES (31, 0, '商户管理', NULL, NULL, 0, 'system', 2, 1);
INSERT INTO `sys_menu` VALUES (32, 31, '公司管理', 'merchant/company', NULL, 1, 'menu', 1, 1);
INSERT INTO `sys_menu` VALUES (34, 32, '新增', NULL, 'merchant:company:save', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (35, 32, '修改', NULL, 'merchant:company:update', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (36, 32, '删除', NULL, 'merchant:company:delete', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (37, 32, '查看', NULL, 'merchant:company:list', 2, NULL, 1, 1);
INSERT INTO `sys_menu` VALUES (38, 31, '店铺管理', 'merchant/shop', NULL, 1, 'menu', 2, 1);
INSERT INTO `sys_menu` VALUES (39, 38, '查看', NULL, 'merchant:shop:list', 2, NULL, 1, 1);
INSERT INTO `sys_menu` VALUES (40, 38, '新增', NULL, 'merchant:shop:save', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (41, 38, '修改', NULL, 'merchant:shop:update', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (42, 38, '删除', NULL, 'merchant:shop:delete', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (43, 31, '员工管理', 'merchant/employee', NULL, 1, 'menu', 3, 1);
INSERT INTO `sys_menu` VALUES (44, 43, '查看', NULL, 'merchant:employee:list', 2, NULL, 1, 1);
INSERT INTO `sys_menu` VALUES (45, 43, '新增', NULL, 'merchant:employee:save', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (46, 43, '修改', NULL, 'merchant:employee:update', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (47, 43, '删除', NULL, 'merchant:employee:delete', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (48, 31, '商品管理', 'merchant/goods', 'merchant:goods:all', 1, 'menu', 4, 1);
INSERT INTO `sys_menu` VALUES (49, 0, '会员管理', NULL, NULL, 0, 'menu', 3, 1);
INSERT INTO `sys_menu` VALUES (50, 49, '会员卡管理', 'member/vipCard', NULL, 1, 'menu', 1, 1);
INSERT INTO `sys_menu` VALUES (51, 50, '查看', NULL, 'member:vipCard:list', 2, NULL, 1, 1);
INSERT INTO `sys_menu` VALUES (52, 50, '新开会员卡', NULL, 'member:vipCard:save', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (53, 50, '充值/续次', NULL, 'member:vipCard:update', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (54, 50, '注销', NULL, 'member:vipCard:delete', 2, NULL, 2, 1);
INSERT INTO `sys_menu` VALUES (55, 49, '操作记录', 'member/vipCardRecord', 'member:vipCardRecord:all', 1, 'menu', 2, 1);
INSERT INTO `sys_menu` VALUES (56, 0, '收银管理', NULL, NULL, 0, 'menu', 1, 1);
INSERT INTO `sys_menu` VALUES (57, 56, '收银前台', 'cash/cash', 'cash:order:save', 1, 'menu', 1, 1);
INSERT INTO `sys_menu` VALUES (58, 56, '收银订单记录', 'cash/order', 'cash:order:list', 1, 'menu', 2, 1);
INSERT INTO `sys_menu` VALUES (59, 56, '删除', NULL, 'cash:order:delete', 2, NULL, 1, 1);
INSERT INTO `sys_menu` VALUES (60, 50, '卡规则设置', NULL, 'member:vipCard:settings', 2, NULL, 5, 1);
INSERT INTO `sys_menu` VALUES (61, 50, '批量导入', NULL, 'member:vipCard:import', 2, NULL, 4, 0);
INSERT INTO `sys_menu` VALUES (62, 0, '统计报表', NULL, NULL, 0, 'menu', 4, 1);
INSERT INTO `sys_menu` VALUES (63, 62, '充值消费记录', 'report/vipCardRecord', 'report:vipCardRecord:all', 1, 'menu', 1, 1);
INSERT INTO `sys_menu` VALUES (64, 62, '会员开卡统计', 'report/vipCard', 'report:vipCard:all', 1, 'menu', 2, 1);
INSERT INTO `sys_menu` VALUES (65, 0, '购买申请', 'serviceApply/serviceApply', 'serviceApply:all', 0, 'menu', 5, 1);
INSERT INTO `sys_menu` VALUES (66, 31, '库存管理', 'merchant/stock', 'merchant:shop:stock', 1, 'menu', 5, 1);
INSERT INTO `sys_menu` VALUES (67, 32, '店铺,员工数量管理', NULL, 'merchant:company:shop:employee:num', 2, NULL, 5, 1);
INSERT INTO `sys_menu` VALUES (68, 1, '通知管理', 'sys/notice', 'sys:notice:all', 1, 'system', 10, 1);
INSERT INTO `sys_menu` VALUES (69, 49, '会员卡注销记录', 'member/vipCardCancelRecord', 'member:vipCard:cancel', 1, 'system', 3, 1);


-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COMMENT = '文件上传';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COMMENT = '角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '公司管理员', '公司管理员角色', 1, '2019-08-29 14:21:48');
INSERT INTO `sys_role` VALUES (2, '员工', '员工角色', 1, '2019-08-29 16:28:25');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 461 CHARACTER SET = utf8 COMMENT = '角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (167, 2, 57);
INSERT INTO `sys_role_menu` VALUES (168, 2, 58);
INSERT INTO `sys_role_menu` VALUES (169, 2, 51);
INSERT INTO `sys_role_menu` VALUES (170, 2, 52);
INSERT INTO `sys_role_menu` VALUES (171, 2, 53);
INSERT INTO `sys_role_menu` VALUES (172, 2, 55);
INSERT INTO `sys_role_menu` VALUES (173, 2, 62);
INSERT INTO `sys_role_menu` VALUES (174, 2, 63);
INSERT INTO `sys_role_menu` VALUES (175, 2, 64);
INSERT INTO `sys_role_menu` VALUES (176, 2, 56);
INSERT INTO `sys_role_menu` VALUES (177, 2, 49);
INSERT INTO `sys_role_menu` VALUES (178, 2, 50);
INSERT INTO `sys_role_menu` VALUES (434, 1, 56);
INSERT INTO `sys_role_menu` VALUES (435, 1, 57);
INSERT INTO `sys_role_menu` VALUES (436, 1, 58);
INSERT INTO `sys_role_menu` VALUES (437, 1, 59);
INSERT INTO `sys_role_menu` VALUES (438, 1, 38);
INSERT INTO `sys_role_menu` VALUES (439, 1, 39);
INSERT INTO `sys_role_menu` VALUES (440, 1, 40);
INSERT INTO `sys_role_menu` VALUES (441, 1, 41);
INSERT INTO `sys_role_menu` VALUES (442, 1, 42);
INSERT INTO `sys_role_menu` VALUES (443, 1, 43);
INSERT INTO `sys_role_menu` VALUES (444, 1, 44);
INSERT INTO `sys_role_menu` VALUES (445, 1, 45);
INSERT INTO `sys_role_menu` VALUES (446, 1, 46);
INSERT INTO `sys_role_menu` VALUES (447, 1, 47);
INSERT INTO `sys_role_menu` VALUES (448, 1, 48);
INSERT INTO `sys_role_menu` VALUES (449, 1, 51);
INSERT INTO `sys_role_menu` VALUES (450, 1, 52);
INSERT INTO `sys_role_menu` VALUES (451, 1, 53);
INSERT INTO `sys_role_menu` VALUES (452, 1, 54);
INSERT INTO `sys_role_menu` VALUES (453, 1, 60);
INSERT INTO `sys_role_menu` VALUES (454, 1, 55);
INSERT INTO `sys_role_menu` VALUES (455, 1, 62);
INSERT INTO `sys_role_menu` VALUES (456, 1, 63);
INSERT INTO `sys_role_menu` VALUES (457, 1, 64);
INSERT INTO `sys_role_menu` VALUES (458, 1, 31);
INSERT INTO `sys_role_menu` VALUES (459, 1, 49);
INSERT INTO `sys_role_menu` VALUES (460, 1, 50);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COMMENT = '系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', 1, 1, '2016-11-11 11:11:11');
-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '用户与角色对应关系';

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '系统用户Token';

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `level` int(4) NOT NULL COMMENT '地区等级',
  `parent_id` int(11) NOT NULL COMMENT '上级id',
  `area_all_id` varchar(255) NOT NULL COMMENT '地区allid',
  `area_all_name` varchar(255) NOT NULL COMMENT '地区全称',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态:1-正常，0-禁用',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `area_all_id`(`area_all_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 820009 CHARACTER SET = utf8 COMMENT = '地区';

-- ----------------------------
-- Table structure for t_company
-- ----------------------------
DROP TABLE IF EXISTS `t_company`;
CREATE TABLE `t_company`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(150) NOT NULL COMMENT '公司名称',
  `company_short_name` varchar(150) NULL DEFAULT NULL COMMENT '公司简称',
  `company_area_code` varchar(255) NULL DEFAULT NULL COMMENT '公司区域code',
  `company_area_name` varchar(255) NULL DEFAULT NULL COMMENT '公司区域信息',
  `company_address` varchar(255) NULL DEFAULT NULL COMMENT '公司详细地址',
  `manager_name` varchar(150) NOT NULL COMMENT '公司管理员姓名',
  `manager_mobile` varchar(15) NULL DEFAULT NULL COMMENT '公司管理员电话',
  `username` varchar(50) NOT NULL COMMENT '后台登录账号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `max_shop_num` int(11) DEFAULT -1 COMMENT '公司下允许的店铺数量，负数代表无限制',
  `max_employee_num` int(11) DEFAULT -1 COMMENT '店铺下最大员工数量：负数代表无限制',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `company_name`(`company_name`) USING BTREE,
  UNIQUE INDEX `manager_mobile`(`manager_mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '公司';

-- ----------------------------
-- Table structure for t_employee
-- ----------------------------
DROP TABLE IF EXISTS `t_employee`;
CREATE TABLE `t_employee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL COMMENT '所属店铺id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `employee_name` varchar(150) NOT NULL COMMENT '员工姓名',
  `employee_mobile` varchar(20) NULL DEFAULT NULL COMMENT '员工电话',
  `username` varchar(50) NOT NULL COMMENT '后台登录账号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gender` tinyint(4) NULL DEFAULT 0 COMMENT '性别：0-未知,1-男,2-女',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `id_number` varchar(50) NULL DEFAULT NULL COMMENT '身份证号',
  `address` varchar(255) NULL DEFAULT NULL COMMENT '住址',
  `emergency_contact` varchar(45) NULL DEFAULT NULL COMMENT '紧急联系人',
  `emergency_contact_phone` varchar(20) NULL DEFAULT NULL COMMENT '紧急联系人电话',
  `level` varchar(50) NULL DEFAULT NULL COMMENT '员工等级',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `employee_mobile`(`employee_mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '店铺员工';

-- ----------------------------
-- Table structure for t_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL COMMENT '商品分类id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_unit` varchar(30) NOT NULL COMMENT '商品单位',
  `goods_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品单价',
  `goods_discount` tinyint NOT NULL DEFAULT 100 COMMENT '商品折扣率',
  `card_type` varchar(30) NULL DEFAULT '1,2,3,4' COMMENT '可用会员卡',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_goods`(`company_id`, `goods_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '商品';

-- ----------------------------
-- Table structure for t_goods_cat
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_cat`;
CREATE TABLE `t_goods_cat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL COMMENT '父级id',
  `all_id` varchar(255) NULL DEFAULT NULL COMMENT 'all id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `cat_name` varchar(150) NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_cat`(`company_id`, `cat_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '商品分类';


-- ----------------------------
-- Table structure for t_member
-- ----------------------------
DROP TABLE IF EXISTS `t_member`;
CREATE TABLE `t_member`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(255) NOT NULL COMMENT '微信openid',
  `mobile` varchar(15) NULL DEFAULT NULL COMMENT '电话',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `openid`(`openid`) USING BTREE,
  UNIQUE INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '用户';

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) NOT NULL COMMENT '订单号',
  `order_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
  `pay_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '现金支付金额',
  `balance_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '充值卡余额抵扣金额',
  `times_cost` int(11) NULL DEFAULT 0 COMMENT '次数消耗',
  `points_cost` int(11) NULL DEFAULT 0 COMMENT '积分消耗',
  `points_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '积分抵扣金额',
  `points_give` int(11) NULL DEFAULT 0 COMMENT '订单赠送积分',
  `vip_card_id` int(11) NULL DEFAULT NULL COMMENT '会员卡id',
  `vip_card_no` varchar(255) NULL DEFAULT NULL COMMENT '会员卡号',
  `vip_card_type` tinyint(4) NULL DEFAULT NULL COMMENT '会员卡类型',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(90) NULL DEFAULT NULL COMMENT '会员姓名',
  `member_mobile` varchar(90) NULL DEFAULT NULL COMMENT '会员电话',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `company_name` varchar(150) NOT NULL COMMENT '所属公司名称',
  `shop_id` int(11) NOT NULL COMMENT '收银店铺id',
  `shop_name` varchar(150) NOT NULL COMMENT '收银店铺名称',
  `operator` varchar(50) NULL DEFAULT NULL COMMENT '操作员',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '收银时间',
  `kaidan_user_id` int(11) NULL DEFAULT NULL COMMENT '开单员id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sn`(`sn`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '收银订单';

-- ----------------------------
-- Table structure for t_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_order_goods`;
CREATE TABLE `t_order_goods`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_unit` varchar(30) NOT NULL COMMENT '商品单位',
  `goods_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品单价',
  `goods_discount` tinyint NOT NULL DEFAULT 100 COMMENT '商品折扣率',
  `goods_num` int(11) NULL DEFAULT 0 COMMENT '商品数量',
  `card_type` varchar(45) NOT NULL COMMENT '可用会员卡类型',
  `goods_cat_id` int(11) NOT NULL COMMENT '商品分类id',
  `goods_cat_name` varchar(255) NOT NULL COMMENT '商品分类名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '收银订单商品信息表';

-- ----------------------------
-- Table structure for t_service_apply
-- ----------------------------
DROP TABLE IF EXISTS `t_service_apply`;
CREATE TABLE `t_service_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL COMMENT '购买的服务',
  `contact_name` varchar(150) NOT NULL COMMENT '联系人',
  `contact_mobile` varchar(20) NOT NULL COMMENT '联系电话',
  `address` varchar(255) NULL DEFAULT NULL COMMENT '地址信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '购买服务申请表';

-- ----------------------------
-- Table structure for t_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_shop`;
CREATE TABLE `t_shop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `shop_name` varchar(150) NOT NULL COMMENT '店铺名称',
  `shop_address` varchar(255) NULL DEFAULT NULL COMMENT '地址',
  `shop_manager` varchar(150) NULL DEFAULT NULL COMMENT '管理人',
  `shop_tel` varchar(255) NULL DEFAULT NULL COMMENT '店铺联系电话',
  `shop_wx` varchar(255) NULL DEFAULT NULL COMMENT '微信',
  `shop_email` varchar(255) NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_shop`(`company_id`, `shop_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '公司店铺';

-- ----------------------------
-- Table structure for t_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `t_sms_code`;
CREATE TABLE `t_sms_code`  (
  `code_key` varchar(50) NOT NULL COMMENT 'key',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`code_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '短信验证码';

-- ----------------------------
-- Table structure for t_vip_card
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_card`;
CREATE TABLE `t_vip_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `shop_id` int(11) NULL DEFAULT NULL COMMENT '开卡店铺',
  `card_no` varchar(255) NOT NULL COMMENT '会员卡号',
  `member_name` varchar(90) NULL DEFAULT NULL COMMENT '会员姓名',
  `member_mobile` varchar(90) NOT NULL COMMENT '会员电话',
  `gender` tinyint(4) NULL DEFAULT 0 COMMENT '性别：0-未知,1-男,2-女',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `card_type` tinyint(4) NOT NULL COMMENT '会员卡类型',
  `card_fee` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '开卡费',
  `card_balance` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '卡余额',
  `card_points` int(11) NULL DEFAULT 0 COMMENT '卡剩余积分',
  `card_times` int(11) NULL DEFAULT 0 COMMENT '卡剩余次数',
  `card_times_type` tinyint(4) NULL DEFAULT NULL COMMENT '次数卡类型:1-有限次数卡,0-不限次数卡',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '开卡日期',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '失效日期',
  `operator` varchar(50) NULL DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_member_type`(`member_mobile`, `company_id`, `card_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '会员卡';

-- ----------------------------
-- Table structure for t_vip_card_cancel_record
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_card_cancel_record`;
CREATE TABLE `t_vip_card_cancel_record`  (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `vip_card_id` int(11) NOT NULL COMMENT '会员卡id',
 `member_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
 `company_id` int(11) NOT NULL COMMENT '所属公司id',
 `shop_id` int(11) NULL DEFAULT NULL COMMENT '开卡店铺',
 `card_no` varchar(255) NOT NULL COMMENT '会员卡号',
 `member_name` varchar(90) NULL DEFAULT NULL COMMENT '会员姓名',
 `member_mobile` varchar(90) NOT NULL COMMENT '会员电话',
 `gender` tinyint(4) NULL DEFAULT 0 COMMENT '性别：0-未知,1-男,2-女',
 `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
 `card_type` tinyint(4) NOT NULL COMMENT '会员卡类型',
 `card_times_type` tinyint(4) NULL DEFAULT NULL COMMENT '次数卡类型:1-有限次数卡,0-不限次数卡',
 `create_time` datetime NULL DEFAULT NULL COMMENT '开卡日期',
 `cancel_time` datetime NULL DEFAULT NULL COMMENT '注销日期',
 `operator` varchar(50) NULL DEFAULT NULL COMMENT '操作员',
 PRIMARY KEY (`id`) USING BTREE,
 KEY(`company_id`),
 KEY(`shop_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '会员卡注销记录';

-- ----------------------------
-- Table structure for t_vip_card_record
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_card_record`;
CREATE TABLE `t_vip_card_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `vip_card_id` int(11) NOT NULL COMMENT '会员卡id',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `shop_id` int(11) NULL DEFAULT NULL COMMENT '操作店铺',
  `card_no` varchar(255) NOT NULL COMMENT '会员卡号',
  `member_name` varchar(90) NULL DEFAULT NULL COMMENT '用户姓名',
  `member_mobile` varchar(90) NOT NULL COMMENT '用户电话',
  `card_type` tinyint(4) NOT NULL COMMENT '会员卡类型',
  `change_type` tinyint(4) NOT NULL COMMENT '变更类型：1-后台调整,2-消费',
  `change_fee` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '变更费用',
  `balance_change` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '余额变更',
  `times_change` int(11) NULL DEFAULT 0 COMMENT '次数变更',
  `times_change_type` tinyint(4) NULL DEFAULT NULL COMMENT '次数卡类型变化:1-不限次变更为有限次,0-有限次变更为不限次',
  `points_change` int(11) NULL DEFAULT 0 COMMENT '积分变更',
  `expire_time_change` varchar(255) NULL DEFAULT NULL COMMENT '有效期变更：原有效期-现有效期',
  `change_desc` varchar(255) NULL DEFAULT NULL COMMENT '变更说明',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '变更时间',
  `operator` varchar(50) NULL DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '会员卡变更记录';

-- ----------------------------
-- Table structure for t_vip_card_rule
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_card_rule`;
CREATE TABLE `t_vip_card_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `company_id` int(11) NOT NULL COMMENT '所属公司id',
  `points_amount` varchar(20) NULL DEFAULT '0' COMMENT '1积分抵扣多少元',
  `give_amount` varchar(20) NULL DEFAULT '0' COMMENT '充值1元赠送多少元',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_id`(`company_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '会员卡规则';

-- ----------------------------
-- table structure for t_depot
-- ----------------------------
DROP TABLE IF EXISTS `t_depot`;
CREATE TABLE `t_depot` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shop_id` int(11) NOT NULL COMMENT '店铺id',
  `name` varchar(50) DEFAULT '' COMMENT '仓库名称',
  `init_complete` smallint(6) DEFAULT 1 COMMENT '库存是否已初始化完成：1-正在初始化，2-已完成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '仓库';

-- ----------------------------
-- table structure for t_stock
-- ----------------------------
DROP TABLE IF EXISTS `t_stock`;
CREATE TABLE `t_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shop_id` int(11) NOT NULL COMMENT '店铺id',
  `depot_id` int(11) NOT NULL COMMENT '仓库id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `num` int(11) DEFAULT 0 COMMENT '库存',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '库存';

-- ----------------------------
-- table structure for t_stock_apply_form
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_apply_form`;
CREATE TABLE `t_stock_apply_form` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `company_id` int(11) DEFAULT NULL COMMENT '所属公司id',
 `out_shop_id` int(11) DEFAULT NULL COMMENT '出货店铺id',
 `in_shop_id` int(11) DEFAULT NULL COMMENT '入货店铺id',
 `in_depot_id` int(11) DEFAULT NULL COMMENT '入货仓库id',
 `out_depot_id` int(11) DEFAULT NULL COMMENT '出货仓库id',
 `type` smallint(6) NOT NULL COMMENT '类型：1-出库单；2-入库单；3-移库单',
 `create_time` datetime DEFAULT NULL COMMENT '申请时间',
 `action_user_id` bigint(20) NOT NULL COMMENT '操作人',
 `verify_time` datetime DEFAULT NULL COMMENT '审核时间',
 `verify_user_id` bigint(20) DEFAULT NULL COMMENT '审核人',
 `status` smallint(6) DEFAULT NULL COMMENT '状态：1-申请中/待审核,2-成功',
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '出入、移库单';

-- ----------------------------
-- table structure for t_stock_apply_form_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_apply_form_goods`;
CREATE TABLE `t_stock_apply_form_goods` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `stock_apply_form_id` int(11) NOT NULL COMMENT '出入、移库单id',
 `goods_id` int(11) NOT NULL COMMENT '商品id',
 `num` int(11) DEFAULT 0 COMMENT '数量',
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '出入、移库单-商品关联关系';

-- ----------------------------
-- table structure for t_stock_record
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_record`;
CREATE TABLE `t_stock_record` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `shop_id` int(11) NOT NULL COMMENT '店铺id',
 `depot_id` int(11) NOT NULL COMMENT '仓库id',
 `goods_id` int(11) NOT NULL COMMENT '商品id',
 `num` int(11) DEFAULT 0 COMMENT '数量',
 `type` smallint(6) NOT NULL COMMENT '类型：1-出库；2-入库；4-销售',
 `form_id` int(11) DEFAULT NULL COMMENT '出入、移库单id，只有库存操作才有',
 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
 PRIMARY KEY (`id`) USING BTREE,
 KEY `stock_record_shop_key`(`shop_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '库存变动记录';

-- ----------------------------
-- table structure for t_notice
-- ----------------------------
DROP TABLE IF EXISTS `t_notice`;
CREATE TABLE `t_notice` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `title` varchar(100) NOT NULL COMMENT '标题',
 `content` varchar(1000) NOT NULL COMMENT '通知内容',
 `order_num` int(11) DEFAULT 1 COMMENT '排序',
 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
 `status` smallint(6) DEFAULT '1' COMMENT '发布状态:1-未发布,2-已发布',
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '系统通知';


-- 商品店铺价格
DROP TABLE IF EXISTS `t_goods_price`;
CREATE TABLE `t_goods_price` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `shop_id` int(11) NOT NULL COMMENT '店铺id',
 `goods_id` int(11) NOT NULL COMMENT '商品id',
 `goods_price` decimal(10,2) default 0.00 COMMENT '商品价格',
 `goods_discount` tinyint NOT NULL DEFAULT 100 COMMENT '商品折扣率',
 PRIMARY KEY (`id`) USING BTREE,
 KEY (`shop_id`),
 KEY (`goods_id`),
 UNIQUE KEY `shop_goods`(`shop_id`, `goods_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '商品店铺价格';

-- 开单员
DROP TABLE IF EXISTS `t_kaidan_user`;
CREATE TABLE `t_kaidan_user` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `company_id` int(11) NOT NULL COMMENT '公司id',
 `name` varchar(20) NOT NULL COMMENT '姓名',
 `mobile` varchar(20) NOT NULL COMMENT '电话',
 PRIMARY KEY (`id`) USING BTREE,
 UNIQUE KEY(`mobile`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '开单员';

SET FOREIGN_KEY_CHECKS = 1;
