package com.glkj.vipsystem.plugin.message;

/**
 * 消息推送模板枚举类
 */
public enum MessageTemplate {

    /**
     * {{first.DATA}} 尊敬的会员，您的次卡已成功消费
     * 消费日期：{{keyword1.DATA}} 消费时间
     * 消费店铺：{{keyword2.DATA}} 店铺
     * 消费次数：{{keyword3.DATA}} 1
     * 剩余次数：{{keyword4.DATA}} 剩余次数
     * {{remark.DATA}} 如有疑问，请联系我们！
     */
    CashCiKa("M1jTQGnuOGyTWhrchAcy0_1pyBNdLOiw7SjcdX1PDMw"),
    /**
     * {{first.DATA}} 尊敬的会员，您的充值卡已成功消费
     * 本次消费金额：{{keyword1.DATA}} 消费金额
     * 消费门店：{{keyword2.DATA}} 门店信息
     * 当前可用余额：{{keyword3.DATA}} 可用余额
     * {{remark.DATA}} 如有疑问，请联系我们！
     */
    CashChongzhi("sNMYhWWGITe4BjxCQCIfgXWxvwWaz6uqkW9Q2XhED10"),
    /**
     * {{first.DATA}} 尊敬的会员，您的积分卡已成功消费
     * 消费时间：{{keyword1.DATA}} 消费时间
     * 消费金额：{{keyword2.DATA}} 消费金额
     * 积分变动：{{keyword3.DATA}} 积分变动
     * 变动原因：{{keyword4.DATA}} 积分消费
     * 本店积分余额：{{keyword5.DATA}} 积分剩余
     * {{remark.DATA}} 如有疑问，请联系我们！
     */
    CashJifen("PQbWtYCCRwCDRuppKQFS3OJ--SwYQ2pKum9fKx3ydmo");


    /**
     * 模板id
     */
    private String id;

    MessageTemplate(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
