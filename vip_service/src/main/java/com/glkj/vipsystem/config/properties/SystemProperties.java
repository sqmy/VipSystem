package com.glkj.vipsystem.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = SystemProperties.PREFIX)
@Getter
@Setter
public class SystemProperties {

    public static final String PREFIX = "system";

    /**
     * 是否开启验证码
     */
    private boolean kaptchaOpen;

    /**
     * 是否开启swagger
     */
    private boolean swaggerOpen;

    /**
     * 项目名称
     */
    private String appName;

    /**
     * 是否开启redis缓存
     */
    private boolean redisOpen;

    /**
     * 是否开启shiro redis 缓存
     */
    private boolean shiroRedis;
}
