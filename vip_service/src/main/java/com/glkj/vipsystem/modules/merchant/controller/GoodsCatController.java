package com.glkj.vipsystem.modules.merchant.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.common.validator.ValidatorUtils;
import com.glkj.vipsystem.entity.ao.GoodsCatAO;
import com.glkj.vipsystem.modules.sys.entity.SysMenuEntity;
import com.glkj.vipsystem.service.IGoodsCatService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 商品分类控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/merchant/goodsCat")
public class GoodsCatController {

    @Resource
    private IGoodsCatService goodsCatService;

    /**
     * 获取公司下的商品分类
     */
    @RequestMapping(value = "/getByCompany")
    @RequiresPermissions(value = {"merchant:goods:all","cash:order:list"}, logical = Logical.OR)
    public R tree(@RequestParam("companyId") Integer companyId) {
        if (companyId == null) {
            return R.ok().put("data", new ArrayList<>());
        }
        List<GoodsCatAO> list = goodsCatService.selectList(
                new EntityWrapper<GoodsCatAO>()
                        .orderBy("id asc")
                        .eq("company_id", companyId)
        );
        return R.ok().put("data", list);
    }

    /**
     * 选择上级分类(添加、修改分类)
     */
    @GetMapping("/selectByCompany")
    @RequiresPermissions("merchant:goods:all")
    public R select(@RequestParam("companyId") Integer companyId) {
        if (companyId == null) {
            return R.ok().put("data", new ArrayList<>());
        }
        //查询列表数据
        List<GoodsCatAO> list = goodsCatService.selectList(
                new EntityWrapper<GoodsCatAO>()
                        .orderBy("id asc")
                        .eq("company_id", companyId)
        );
        //添加顶级菜单
        GoodsCatAO root = new GoodsCatAO();
        root.setId(0);
        root.setCatName("一级");
        list.add(root);

        return R.ok().put("data", list);
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{id}")
    @RequiresPermissions({"merchant:goods:all"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", goodsCatService.selectById(id));
    }

    /**
     * 保存新增
     */
    @RequestMapping(value = "/save")
    @RequiresPermissions({"merchant:goods:all"})
    public R save(@RequestBody GoodsCatAO goodsCat) {
        ValidatorUtils.validateEntity(goodsCat);
        if(goodsCat.getParentId() == null){
            goodsCat.setParentId(0);
        }
        goodsCatService.save(goodsCat);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @RequiresPermissions({"merchant:goods:all"})
    public R update(@RequestBody GoodsCatAO goodsCat) {
        // 商品分类修改 只能修改名称
        goodsCat.setAllId(null);
        goodsCat.setParentId(null);
        goodsCat.setCompanyId(null);
        try {
            goodsCatService.updateById(goodsCat);
        }catch (DuplicateKeyException e){
            throw new BizException("分类名称重复");
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @RequiresPermissions({"merchant:goods:all"})
    public R delete(@RequestBody Integer[] ids) {
        if (ids != null && ids.length > 0) {
            goodsCatService.deleteBatch(Arrays.asList(ids));
        }
        return R.ok();
    }
}
