package com.glkj.vipsystem.modules.app.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.common.utils.HttpContextUtils;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.entity.ao.MemberAO;
import com.glkj.vipsystem.modules.app.interceptor.AuthorizationInterceptor;
import com.glkj.vipsystem.service.ICompanyService;
import com.glkj.vipsystem.service.IEmployeeService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

public abstract class AbstractAppController {

    @Resource
    private ICompanyService companyService;
    @Resource
    private IEmployeeService employeeService;

    protected String getOpenid() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        String openid = (String) request.getAttribute("openid");
        return openid;
    }

    /**
     * 校验用户是否有商家端的权限
     */
    protected void validRole() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        MemberAO member = (MemberAO) request.getAttribute(AuthorizationInterceptor.USER_KEY);
        String mobile = member.getMobile();
        CompanyAO companyAO = companyService.selectOne(
                new EntityWrapper<CompanyAO>()
                        .eq("manager_mobile", mobile)
        );
        EmployeeAO employeeAO = employeeService.selectOne(
                new EntityWrapper<EmployeeAO>()
                        .eq("employee_mobile", mobile)
        );
        if (companyAO == null && employeeAO == null) {
            throw new BizException("无权限", 999);
        }

    }

}
