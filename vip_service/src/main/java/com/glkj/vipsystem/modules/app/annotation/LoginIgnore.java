package com.glkj.vipsystem.modules.app.annotation;

import java.lang.annotation.*;

/**
 * app登录忽略效验
 *
 * @author LiMuchan
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginIgnore {
}
