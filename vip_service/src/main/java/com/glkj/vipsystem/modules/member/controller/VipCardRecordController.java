package com.glkj.vipsystem.modules.member.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.VipCardRecordAO;
import com.glkj.vipsystem.modules.sys.controller.AbstractController;
import com.glkj.vipsystem.service.IVipCardRecordService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 会员卡变更记录控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/member/vipCardRecord")
public class VipCardRecordController extends AbstractController {

    @Resource
    private IVipCardRecordService vipCardRecordService;

    /**
     * 列表
     */
    @RequestMapping(value = "/list")
    @RequiresPermissions({"member:vipCardRecord:all"})
    public R list(@RequestParam Map<String, Object> params) {
        Wrapper<VipCardRecordAO> wrapper = getWrapper(params);
        Page<VipCardRecordAO> page = vipCardRecordService.selectPage(
                new Query<VipCardRecordAO>(params).getPage(), wrapper
        );
        return R.ok().put("page", new PageUtils(page));
    }

    /**
     * 计算开续卡收费累计
     */
    @RequestMapping(value = "/getMoney")
    @RequiresPermissions({"member:vipCardRecord:all"})
    public R getMoney(@RequestParam Map<String, Object> params) {
        Wrapper<VipCardRecordAO> wrapper = getWrapper(params);
        wrapper.setSqlSelect("sum(change_fee) as money");
        Map<String, Object> data = vipCardRecordService.selectMap(
                wrapper
        );
        if (data == null) {
            data = new HashMap<>();
        }
        data.putIfAbsent("money", "0");
        return R.ok().put("data", data);
    }

    private Wrapper<VipCardRecordAO> getWrapper(Map<String, Object> params) {
        String memberInfo = (String) params.get("memberInfo");
        String time = (String) params.get("time");
        String cardType = (String) params.get("cardType");
        Wrapper<VipCardRecordAO> wrapper = new EntityWrapper<VipCardRecordAO>();
        wrapper.orderBy("id desc");
        // 只看调整和新开
        wrapper.ne("change_type", Constant.VipCardRecordChangeType.COST.getValue());
        // 数据权限控制
        if (isCompanyRole()) {
            // 公司管理员
            String username = getUser().getUsername();
            wrapper.where("company_id = (select id from t_company where `username` = '" + username + "')");
        } else if (isEmployeeRole()) {
            // 员工
            String username = getUser().getUsername();
            wrapper.where("company_id = (select company_id from t_employee where `username` = '" + username + "')");
        }

        if (StringUtils.isNotBlank(memberInfo)) {
            wrapper.where("(member_name like '%" + memberInfo + "%' or member_mobile like '%" + memberInfo + "%'"
                    + " or card_no like '%" + memberInfo + "%')");
        }
        if (StringUtils.isNotBlank(cardType)) {
            wrapper.eq("card_type", cardType);
        }
        if (StringUtils.isNotBlank(time) && time.contains("~")) {
            String[] timeRange = time.split("~");
            wrapper.ge("create_time", timeRange[0].trim());
            wrapper.le("create_time", timeRange[1].trim() + " 23:59:59");
        }
        return wrapper;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{id}")
    @RequiresPermissions({"member:vipCardRecord:all"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", vipCardRecordService.selectById(id));
    }

}
