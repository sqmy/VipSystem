package com.glkj.vipsystem.modules;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.ALWAYS)
@ApiModel(value = "ServiceResult", description = "通用返回对象")
public class ServiceResult<T> implements Serializable {

    public static final int SUCCESS = 200;
    public static final int ERROR = 500;
    public static final int NO_LOGIN = -1;

    public static final String SUCCESS_MESSAGE = "成功";
    public static final String ERROR_MESSAGE = "error";
    public static final String NO_LOGIN_MESSAGE = "未登录或登录超时";

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 6977558218691386450L;

    /**
     * 返回码
     */
    @ApiModelProperty(value = "状态码", name = "code",example = "" + SUCCESS)
    private int code = SUCCESS;

    /**
     * 提示信息
     */
    @ApiModelProperty(value = "响应消息,如果code是200,返回正确消息,否则错误消息", name = "msg", example = SUCCESS_MESSAGE)
    private String msg;

    /**
     * 数据对象
     */
    @ApiModelProperty(value = "调用成功,返回的数据", name = "data")
    private T data;

    /**
     * 所有额外的列属性
     */
    @ApiModelProperty(value = "调用成功,附加返会的数据", name = "additionalProperties")
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public ServiceResult() {
    }

    public ServiceResult(int code, String msg, T data) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public ServiceResult(T data) {
        this.data = data;
    }

    public ServiceResult(boolean succeed, int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ServiceResult(boolean succeed, T data, String msg) {
        this.data = data;
        this.msg = msg;
    }

    public ServiceResult(String msg) {
        this.msg = msg;
    }

    public static <T> ServiceResult<T> ok() {
        return new ServiceResult<T>(SUCCESS, SUCCESS_MESSAGE, null);
    }

    public static <T> ServiceResult<T> ok(T data) {
        return new ServiceResult<T>(SUCCESS, SUCCESS_MESSAGE, data);
    }

    public static <T> ServiceResult<T> error() {
        return new ServiceResult<T>(ERROR, ERROR_MESSAGE, null);
    }

    public static <T> ServiceResult<T> error(String msg) {
        return new ServiceResult<T>(ERROR, msg, null);
    }

    public static <T> ServiceResult<T> error(int code, String msg) {
        return new ServiceResult<T>(code, msg, null);
    }

    public static <T> ServiceResult<T> noLogin() {
        return new ServiceResult<T>(NO_LOGIN, NO_LOGIN_MESSAGE, null);
    }

    /**
     * @param data                 数据
     * @param additionalProperties 扩展返回参数集 Map<String,Object>
     * @return ServiceResult
     */
    public static <T> ServiceResult ok(T data, Map<String, Object> additionalProperties) {
        ServiceResult<T> result = ok(data);
        result.additionalProperties.putAll(additionalProperties);
        return result;
    }

    public boolean isSucceed() {
        return this.code == SUCCESS;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        if (this.additionalProperties != null) {
            this.additionalProperties.putAll(additionalProperties);
        } else {
            this.additionalProperties = additionalProperties;
        }
    }

    public Object getAdditionalProperties(String name) {
        return this.additionalProperties.get(name);
    }

}
