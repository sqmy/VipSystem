package com.glkj.vipsystem.modules.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.AreaAO;
import com.glkj.vipsystem.service.IAreaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 区域控制器
 */
@RestController
@RequestMapping("/sys/area")
public class AreaController {

    @Resource
    private IAreaService areaService;

    /**
     * 区域树
     */
    @RequestMapping("/tree")
    public R tree() {
        List<AreaAO> areaAOS = areaService.selectList(new EntityWrapper<AreaAO>());
        AreaAO areaAO = new AreaAO();
        areaAO.setAreaAllId(null);
        areaAO.setId(0);
        areaAO.setAreaAllName("全部");
        areaAO.setName("全部");
        areaAO.setLevel(0);
        areaAO.setStatus(1);
        areaAOS.add(areaAO);
        return R.ok().put("data", areaAOS);
    }

}
