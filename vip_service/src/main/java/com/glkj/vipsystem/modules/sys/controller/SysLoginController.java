package com.glkj.vipsystem.modules.sys.controller;

import cn.hutool.core.lang.Validator;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.common.validator.ValidatorUtils;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import com.glkj.vipsystem.core.sms.aliyun.AliyunSmsTemplate;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.entity.ao.ShopAO;
import com.glkj.vipsystem.modules.sys.entity.SysUserEntity;
import com.glkj.vipsystem.modules.sys.form.SysLoginForm;
import com.glkj.vipsystem.modules.sys.form.SysSmsForm;
import com.glkj.vipsystem.modules.sys.service.SysCaptchaService;
import com.glkj.vipsystem.modules.sys.service.SysUserService;
import com.glkj.vipsystem.modules.sys.service.SysUserTokenService;
import com.glkj.vipsystem.service.ICompanyService;
import com.glkj.vipsystem.service.IEmployeeService;
import com.glkj.vipsystem.service.IShopService;
import com.glkj.vipsystem.service.ISmsCodeService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * 登录相关
 *
 * @author LiMuchan
 * @date 2016年11月10日 下午1:15:31
 */
@RestController
public class SysLoginController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserTokenService sysUserTokenService;
    @Autowired
    private SysCaptchaService sysCaptchaService;
    @Resource
    private ICompanyService companyService;
    @Resource
    private IShopService shopService;
    @Resource
    private IEmployeeService employeeService;
    @Resource
    private ISmsCodeService smsCodeService;

    /**
     * 验证码
     */
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid) throws ServletException, IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //获取图片验证码
        BufferedImage image = sysCaptchaService.getCaptcha(uuid);

        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }

    /**
     * 登录
     */
    @PostMapping("/sys/login")
    public Map<String, Object> login(@RequestBody SysLoginForm form) throws IOException {
        boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
        if (!captcha) {
            return R.error("验证码不正确");
        }

        //用户信息
        SysUserEntity user = sysUserService.queryByUserName(form.getUsername());

        //账号不存在、密码错误
        if (user == null || !user.getPassword().equals(new Sha256Hash(form.getPassword(), user.getSalt()).toHex())) {
            return R.error("账号或密码不正确");
        }

        //账号锁定
        if (user.getStatus() == 0) {
            return R.error("账号已被锁定,请联系管理员");
        }

        // 员工 公司/店铺被禁用 员工不能登录后台
        if (isEmployeeRole(user)) {
            EmployeeAO employeeAO = employeeService.selectByUsername(user.getUsername());
            if (employeeAO == null) {
                return R.error("账号或密码不正确");
            }
            int count = shopService.selectCount(
                    new EntityWrapper<ShopAO>()
                            .eq("id", employeeAO.getShopId())
                            .eq("status", 1)
            );
            if (count == 0) {
                return R.error("账号已被锁定,请联系管理员");
            }
            count = companyService.selectCount(
                    new EntityWrapper<CompanyAO>()
                            .eq("id", employeeAO.getCompanyId())
                            .eq("status", 1)
            );
            if (count == 0) {
                return R.error("账号已被锁定,请联系管理员");
            }
        }

        //生成token，并保存到数据库
        R r = sysUserTokenService.createToken(user.getUserId());
        return r;
    }


    /**
     * 退出
     */
    @PostMapping("/sys/logout")
    public R logout() {
        sysUserTokenService.logout(getUserId());
        return R.ok();
    }

    /**
     * 发送短信验证码
     */
    @PostMapping("/sys/sms/send")
    public R sendSmsCode(@RequestBody SysSmsForm form) {
        boolean validate = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
        if (!validate) {
            return R.error("图片验证码不正确");
        }
        String mobile = form.getMobile();
        if (!Validator.isMobile(mobile)) {
            return R.error("请输入正确的手机号");
        }
        Constant.SmsCodeType codeType = Constant.SmsCodeType.typeOf(form.getType());
        if (codeType == null) {
            return R.error("类型错误");
        }

        // 发送验证码 300s = 5 分钟过期
        smsCodeService.send(AliyunSmsTemplate.NewPassword, codeType, mobile, 300);
        return R.ok();
    }

    /**
     * 公司管理员找回密码
     */
    @RequestMapping(value = "/sys/password/new", method = RequestMethod.POST)
    public R check(@RequestBody CompanyAO company) {
        ValidatorUtils.validateEntity(company, UpdateGroup.class);
        CompanyAO companyAO = companyService.selectOne(
                new EntityWrapper<CompanyAO>()
                        .eq("company_name", company.getCompanyName())
                        .eq("manager_name", company.getManagerName())
                        .eq("manager_mobile", company.getManagerMobile())
                        .eq("status", 1)
        );
        if (companyAO == null) {
            return R.error("公司不存在或已禁用");
        }
        String password = company.getPassword();
        String confirmPassword = company.getConfirmPassword();
        if (StringUtils.isBlank(password)) {
            return R.error("新密码不能为空");
        } else if (!password.equals(confirmPassword)) {
            return R.error("确认密码不一致");
        }
        if (StringUtils.isBlank(company.getCode())){
            return R.error("验证码不能为空");
        }
        boolean validate = smsCodeService.validate(Constant.SmsCodeType.SysFindPassword, company.getManagerMobile(), company.getCode());
        if (!validate){
            return R.error("验证码错误");
        }
        company.setId(companyAO.getId());
        company.setUsername(companyAO.getUsername());
        companyService.updatePassword(company);
        return R.ok();
    }

}
