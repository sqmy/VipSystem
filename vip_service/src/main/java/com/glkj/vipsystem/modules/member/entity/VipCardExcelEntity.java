package com.glkj.vipsystem.modules.member.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会员卡导入Excel对应实体
 * @author limuchan
 */
@Data
public class VipCardExcelEntity implements Serializable {

    @Excel(name = "用户姓名", isImportField = "true")
    private String memberName;

    @Excel(name = "用户电话", isImportField = "true")
    private String memberMobile;

    @Excel(name = "卡类型", replace = {"次卡_1","充值卡_2","积分卡_3"}, isImportField = "true")
    private Integer cardType;

    @Excel(name = "开卡费用", isImportField = "true")
    private String cardFee;

    @Excel(name = "余额/积分/次数", isImportField = "true")
    private String value;

    @Excel(name = "是否限次", replace = {"否_0","是_1"})
    private Integer cardTimesType;

    @Excel(name = "失效日期", importFormat = "yyyy-MM-dd")
    private Date expireTime;

}
