package com.glkj.vipsystem.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "基础表单信息")
public class BaseForm {

    @ApiModelProperty(value = "page")
    private Integer page = 1;

    @ApiModelProperty(value = "pageSize")
    private Integer pageSize = 10;

}
