package com.glkj.vipsystem.modules.merchant.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.entity.ao.GoodsAO;
import com.glkj.vipsystem.entity.gen.Depot;
import com.glkj.vipsystem.entity.gen.Stock;
import com.glkj.vipsystem.modules.sys.controller.AbstractController;
import com.glkj.vipsystem.service.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 库存变动记录控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/merchant/shop/stock")
public class StockController extends AbstractController {

    @Resource
    private IShopService shopService;
    @Resource
    private IEmployeeService employeeService;

    @Resource
    private IStockService stockService;
    @Resource
    private IGoodsService goodsService;
    @Resource
    private IDepotService depotService;
    @Resource
    private ICompanyService companyService;

    /**
     * 获取店铺库存列表
     */
    @GetMapping(value = "/list")
    @RequiresPermissions({"merchant:shop:stock"})
    public R stock(@RequestParam Map<String, Object> params) {
        Integer shopId = null;
        String shopIdStr = (String) params.get("shopId");
        String goodsName = (String) params.get("goodsName");
        if (StringUtils.isNotBlank(shopIdStr)) {
            shopId = Integer.valueOf(shopIdStr);
        }
        Integer companyId = null;
        String username = getUser().getUsername();
        if (isCompanyRole()) {
            CompanyAO companyAO = companyService.selectByUsername(username);
            companyId = companyAO.getId();
        } else if (isEmployeeRole()) {
            EmployeeAO employeeAO = employeeService.selectByUsername(username);
            shopId = employeeAO.getShopId();
        }
        Page<Stock> page = stockService.selectShopStocks(
                new Query<Stock>(params).getPage(),
                companyId, shopId, StringUtils.isNotBlank(goodsName) ? goodsName : null);
        return R.ok().put("page", page);
    }

    /**
     * 商品库存
     */
    @RequestMapping(value = "/goods-list")
    @RequiresPermissions(value = {"merchant:shop:stock"}, logical = Logical.OR)
    public R goodsList(@RequestParam Map<String, Object> params) {
        String companyId = (String) params.get("companyId");
        String shopId = (String) params.get("shopId");
        if (StringUtils.isBlank(companyId)) {
            return R.ok().put("page", new PageUtils(new Page<>()));
        }
        String goodsCatId = (String) params.get("goodsCatId");
        String goodsName = (String) params.get("goodsName");
        Wrapper<GoodsAO> wrapper = new EntityWrapper<>();
        wrapper.orderBy("id desc");
        wrapper.setSqlSelect("*,(select cat_name from t_goods_cat where id = t_goods.cat_id) as cat_name");
        wrapper.eq("company_id", Integer.valueOf(companyId));

        if (StringUtils.isNotBlank(goodsCatId)) {
            wrapper.where("cat_id in (select id from t_goods_cat where FIND_IN_SET('" + goodsCatId + "',`all_id`))");
        }
        if (StringUtils.isNotBlank(goodsName)) {
            wrapper.like("goods_name", goodsName);
        }
        Page<GoodsAO> page = goodsService.selectPage(
                new Query<GoodsAO>(params).getPage(), wrapper
        );
        List<GoodsAO> records = page.getRecords();
        if (!CollectionUtils.isEmpty(records) && StringUtils.isNotBlank(shopId)) {
            // 库存数量
            Depot depot = depotService.selectOne(
                    new EntityWrapper<Depot>()
                            .eq("shop_id", shopId)
                            .last("limit 1")
            );
            if (depot != null) {
                Integer depotId = depot.getId();
                for (GoodsAO record : records) {
                    Stock stock = stockService.selectOne(
                            new EntityWrapper<Stock>()
                                    .eq("shop_id", shopId)
                                    .eq("depot_id", depotId)
                                    .eq("goods_id", record.getId())
                    );
                    if (stock != null) {
                        record.setNum(stock.getNum());
                    }
                }
            }
        }
        return R.ok().put("page", new PageUtils(page));
    }
}
