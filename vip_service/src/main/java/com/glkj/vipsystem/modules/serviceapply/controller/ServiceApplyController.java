package com.glkj.vipsystem.modules.serviceapply.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.ServiceApplyAO;
import com.glkj.vipsystem.service.IServiceApplyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 购买服务申请表控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/serviceApply")
public class ServiceApplyController {

    @Resource
    private IServiceApplyService serviceApplyService;

    /**
     * 列表
     */
    @RequestMapping(value = "/list")
    @RequiresPermissions({"serviceApply:all"})
    public R list(@RequestParam Map<String, Object> params) {
        Page<ServiceApplyAO> page = serviceApplyService.selectPage(
                new Query<ServiceApplyAO>(params).getPage(),
                new EntityWrapper<ServiceApplyAO>()
                        .orderBy("id desc")
        );
        return R.ok().put("page", new PageUtils(page));
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{id}")
    @RequiresPermissions({"serviceApply:all"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", serviceApplyService.selectById(id));
    }


    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @RequiresPermissions({"serviceApply:all"})
    public R update(@RequestBody ServiceApplyAO serviceApply) {
        serviceApplyService.updateById(serviceApply);
        return R.ok();
    }

}
