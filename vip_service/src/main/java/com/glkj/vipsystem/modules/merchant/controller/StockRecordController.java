package com.glkj.vipsystem.modules.merchant.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.enums.StockRecordType;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.entity.gen.StockRecord;
import com.glkj.vipsystem.modules.sys.controller.AbstractController;
import com.glkj.vipsystem.service.ICompanyService;
import com.glkj.vipsystem.service.IEmployeeService;
import com.glkj.vipsystem.service.IStockRecordService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 库存变动记录控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/merchant/shop/stock/record")
public class StockRecordController extends AbstractController {

    @Resource
    private IStockRecordService stockRecordService;
    @Resource
    private ICompanyService companyService;
    @Resource
    private IEmployeeService employeeService;

    /**
     * 列表
     */
    @GetMapping(value = "/list")
    @RequiresPermissions({"merchant:shop:stock"})
    public R list(@RequestParam Map<String, Object> params) {
        String shopName = (String) params.get("shopName");
        String goodsName = (String) params.get("goodsName");
        String time = (String) params.get("time");
        String type = (String) params.get("type");
        Wrapper<StockRecord> wrapper = new EntityWrapper<StockRecord>();
        wrapper.orderBy("id desc");
        wrapper.setSqlSelect("*" +
                ",(select goods_name from t_goods where id = t_stock_record.goods_id) as goodsName" +
                ",(select shop_name from t_shop where id = t_stock_record.shop_id) as shopName");
        if (StringUtils.isNotBlank(shopName)) {
            wrapper.where("shop_id in (select id from t_shop where shop_name like '%" + shopName + "%')");
        }
        if (StringUtils.isNotBlank(goodsName)) {
            wrapper.where("goods_id in (select id from t_goods where goods_name like '%" + goodsName + "%')");
        }
        if (StringUtils.isNotBlank(time) && time.contains("~")) {
            String[] times = time.split("~");
            wrapper.ge("create_time", times[0].trim());
            wrapper.le("create_time", times[1].trim() + " 23:59:59");
        }
        if (StringUtils.isNotBlank(type)) {
            wrapper.eq("type", type);
        }

        String username = getUser().getUsername();
        // 数据权限控制
        if (isCompanyRole()) {
            CompanyAO companyAO = companyService.selectByUsername(username);
            wrapper.where("shop_id in (select id from t_shop where company_id = '" + companyAO.getId() + "')");
        } else if (isEmployeeRole()) {
            EmployeeAO employeeAO = employeeService.selectByUsername(username);
            wrapper.where("shop_id = '" + employeeAO.getShopId() + "'");
        }

        Page<StockRecord> page = stockRecordService.selectPage(
                new Query<StockRecord>(params).getPage(),
                wrapper
        );
        return R.ok().put("page", new PageUtils(page));
    }

    /**
     * 详情
     */
    @GetMapping(value = "/detail/{id}")
    @RequiresPermissions({"merchant:shop:stock"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", stockRecordService.selectById(id));
    }

    /**
     * 获取库存记录类型
     */
    @GetMapping(value = "/type")
    @RequiresPermissions({"merchant:shop:stock"})
    public R type() {
        List<JSONObject> data = new ArrayList<>();
        StockRecordType[] values = StockRecordType.values();

        for (StockRecordType value : values) {
            JSONObject type = new JSONObject();
            type.put("label", value.getMsg());
            type.put("value", value.getType());
            data.add(type);
        }
        return R.ok().put("data", data);
    }
}
