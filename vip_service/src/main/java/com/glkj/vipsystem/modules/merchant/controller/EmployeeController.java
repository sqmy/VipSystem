package com.glkj.vipsystem.modules.merchant.controller;

import cn.hutool.core.lang.Validator;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.common.validator.ValidatorUtils;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.modules.sys.controller.AbstractController;
import com.glkj.vipsystem.service.IEmployeeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * 店铺员工控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/merchant/employee")
public class EmployeeController extends AbstractController {

    @Resource
    private IEmployeeService employeeService;

    /**
     * 列表
     */
    @RequestMapping(value = "/list")
    @RequiresPermissions({"merchant:employee:list"})
    public R list(@RequestParam Map<String, Object> params) {
        String nameOrMobile = (String) params.get("nameOrMobile");
        String shopName = (String) params.get("shopName");
        String companyId = (String) params.get("companyId");
        Wrapper<EmployeeAO> wrapper = new EntityWrapper<>();
        wrapper.orderBy("id desc");
        wrapper.setSqlSelect("*,(select shop_name from t_shop where id = t_employee.shop_id) as shop_name" +
                ",(select company_name from t_company where id = t_employee.company_id) as company_name");

        // 数据权限控制
        if (isCompanyRole()) {
            // 公司管理员
            String username = getUser().getUsername();
            wrapper.where("company_id = (select id from t_company where `username` = '" + username + "')");
        } else if(StringUtils.isNotBlank(companyId)) {
            wrapper.eq("company_id", companyId);
        }

        // 员工姓名或电话搜索
        if (StringUtils.isNotBlank(nameOrMobile)) {
            wrapper.where("(employee_name like '%" + nameOrMobile + "%'" +
                    " or employee_mobile like '%" + nameOrMobile + "%')");
        }
        // 店铺名称搜索
        if (StringUtils.isNotBlank(shopName)) {
            wrapper.where("shop_id in (select id from t_shop where shop_name like '%" + shopName + "%')");
        }

        Page<EmployeeAO> page = employeeService.selectPage(
                new Query<EmployeeAO>(params).getPage(),
                wrapper
        );
        return R.ok().put("page", new PageUtils(page));
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{id}")
    @RequiresPermissions({"merchant:employee:list"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", employeeService.selectById(id));
    }

    /**
     * 保存新增
     */
    @RequestMapping(value = "/save")
    @RequiresPermissions({"merchant:employee:save"})
    public R save(@RequestBody EmployeeAO employee) {
        ValidatorUtils.validateEntity(employee, AddGroup.class);
        if(!employee.getPassword().equals(employee.getConfirmPassword())){
            return R.error("两次密码不一致");
        }
        if (!Validator.isGeneral(employee.getUsername(), 3, 15)) {
            return R.error("请填写3~15位的登录用户名,字母、数字、下划线组合");
        }
        if (!Validator.isGeneral(employee.getPassword(), 6, 15)) {
            return R.error("请填写6~15位的登录密码,字母、数字、下划线组合");
        }
        employeeService.save(employee, getUserId());
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @RequiresPermissions({"merchant:employee:update"})
    public R update(@RequestBody EmployeeAO employee) {
        ValidatorUtils.validateEntity(employee, UpdateGroup.class);
        if (StringUtils.isNotBlank(employee.getPassword()) && !employee.getPassword().equals(employee.getConfirmPassword())) {
            return R.error("两次密码不一致");
        }
        if (StringUtils.isNotBlank(employee.getPassword()) && !Validator.isGeneral(employee.getPassword(), 6, 15)) {
            return R.error("请填写6~15位的登录密码,字母、数字、下划线组合");
        }
        employeeService.update(employee);
        return R.ok();
    }

    /**
     * 修改状态
     */
    @RequestMapping(value = "/updateStatus")
    @RequiresPermissions({"merchant:employee:update"})
    public R updateStatus(@RequestBody EmployeeAO employee) {
        employeeService.update(employee);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @RequiresPermissions({"merchant:employee:delete"})
    public R delete(@RequestBody Integer[] ids) {
        if (ids != null && ids.length > 0) {
            employeeService.deleteBatch(Arrays.asList(ids));
        }
        return R.ok();
    }
}
