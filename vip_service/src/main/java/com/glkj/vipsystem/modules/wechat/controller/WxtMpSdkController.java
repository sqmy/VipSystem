package com.glkj.vipsystem.modules.wechat.controller;

import com.glkj.vipsystem.config.wechat.WxMpConfiguration;
import com.glkj.vipsystem.modules.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "微信公众号-API")
@RestController
@RequestMapping("/wx/mp/jsdk")
@Slf4j
public class WxtMpSdkController {

    @Resource
    private WxMpService wxMpService;

    @ApiOperation(value = "获取公众号sdk信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "url", dataType = "string", paramType = "query", required = true)
    })
    @GetMapping("/init")
    public ServiceResult<WxJsapiSignature> init(@RequestParam String url) {
        if (StringUtils.isBlank(url)) {
            return ServiceResult.error("url空");
        }
        log.debug("进入后台JSSDK获取配置数据 {}", url);
        try {
            WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(url);
            log.debug("进入后台JSSDK获取配置数据 {}", jsapiSignature);
            return ServiceResult.ok(jsapiSignature);
        } catch (WxErrorException e) {
            log.error(e.getMessage(),e);
            e.printStackTrace();
        }
        return null;
    }

}
