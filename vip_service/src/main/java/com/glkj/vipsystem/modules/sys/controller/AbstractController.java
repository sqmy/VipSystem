package com.glkj.vipsystem.modules.sys.controller;

import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.modules.sys.entity.SysUserEntity;
import com.glkj.vipsystem.modules.sys.service.SysUserRoleService;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Controller公共组件
 * 
 * @author LiMuchan
 *
 * @date 2016年11月9日 下午9:42:26
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private SysUserRoleService sysUserRoleService;
	
	protected SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}

	protected boolean isCompanyRole(){
		Long userId = getUserId();
		if(userId == Constant.SUPER_ADMIN){
			return false;
		}
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		if(CollectionUtils.isEmpty(roleIdList)){
			return false;
		}
		return roleIdList.contains(Constant.Role.COMPANY.getValue());
	}

	protected boolean isEmployeeRole(){
		Long userId = getUserId();
		if(userId == Constant.SUPER_ADMIN){
			return false;
		}
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		if(CollectionUtils.isEmpty(roleIdList)){
			return false;
		}
		return roleIdList.contains(Constant.Role.EMPLOYEE.getValue());
	}

	protected boolean isEmployeeRole(SysUserEntity userEntity){
		Long userId = userEntity.getUserId();
		if(userId == Constant.SUPER_ADMIN){
			return false;
		}
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		if(CollectionUtils.isEmpty(roleIdList)){
			return false;
		}
		return roleIdList.contains(Constant.Role.EMPLOYEE.getValue());
	}

}
