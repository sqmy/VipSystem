package com.glkj.vipsystem.modules.sys.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 发送验证码
 *
 * @author LiMuchan
 */
@Data
@ApiModel(value = "发送验证码")
public class SysSmsForm {

    @NotBlank(message = "手机号空")
    private String mobile;

    @NotNull(message = "请选择类型")
    private Integer type;

    @NotBlank(message = "图片验证码空")
    private String captcha;

    @NotBlank(message = "uuid空")
    private String uuid;
}
