package com.glkj.vipsystem.modules.member.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.entity.ao.VipCardAO;
import com.glkj.vipsystem.entity.gen.VipCardCancelRecord;
import com.glkj.vipsystem.modules.sys.controller.AbstractController;
import com.glkj.vipsystem.service.IVipCardCancelRecordService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 会员卡注销记录控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/member/vipCardCancelRecord")
public class VipCardCancelRecordController extends AbstractController {

    @Resource
    private IVipCardCancelRecordService vipCardCancelRecordService;

    /**
     * 列表
     */
    @GetMapping(value = "/list")
    @RequiresPermissions({"member:vipCard:cancel"})
    public R list(@RequestParam Map<String, Object> params) {
        String companyId = (String) params.get("companyId"); // 所属公司筛选
        String shopId = (String) params.get("shopId"); // 开卡店铺筛选
        String cardNo = (String) params.get("cardNo"); // 会员卡号搜索
        String memberInfo = (String) params.get("memberInfo"); // 会员姓名/电话搜索
        String cardType = (String) params.get("cardType"); // 卡类型搜索

        Wrapper<VipCardCancelRecord> wrapper = new EntityWrapper<VipCardCancelRecord>();
        wrapper.orderBy("cancel_time desc");
        wrapper.setSqlSelect("*,(select company_name from t_company where id = t_vip_card_cancel_record.company_id) as companyName" +
                ",(select shop_name from t_shop where id = t_vip_card_cancel_record.shop_id) as shopName");

        // 数据权限控制
        if (isCompanyRole()) {
            // 公司管理员
            String username = getUser().getUsername();
            wrapper.where("company_id = (select id from t_company where `username` = '" + username + "')");
        } else if (isEmployeeRole()) {
            // 员工
            String username = getUser().getUsername();
            wrapper.where("shop_id = (select shop_id from t_employee where `username` = '" + username + "')");
        }
        if (StringUtils.isNotBlank(companyId)) {
            wrapper.eq("company_id", companyId);
        }
        if (!isEmployeeRole() && StringUtils.isNotBlank(shopId)) {
            wrapper.eq("shop_id", shopId);
        }
        if (StringUtils.isNotBlank(cardNo)) {
            wrapper.like("card_no", cardNo);
        }
        if (StringUtils.isNotBlank(memberInfo)) {
            wrapper.where("(member_name like '%" + memberInfo + "%' or member_mobile like '%" + memberInfo + "%')");
        }
        if (StringUtils.isNotBlank(cardType)) {
            wrapper.eq("card_type", cardType);
        }
        Page<VipCardCancelRecord> page = vipCardCancelRecordService.selectPage(
                new Query<VipCardCancelRecord>(params).getPage(),
                wrapper
        );
        return R.ok().put("page", new PageUtils(page));
    }

}
