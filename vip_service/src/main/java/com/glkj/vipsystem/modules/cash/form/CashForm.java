package com.glkj.vipsystem.modules.cash.form;

import com.glkj.vipsystem.entity.ao.GoodsAO;
import com.glkj.vipsystem.entity.ao.VipCardAO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 收银表单
 */
@Data
public class CashForm {
    /**
     * 订单编号
     */
    @NotBlank(message = "订单编号空")
    private String sn;

    /**
     * 公司id
     */
    private Integer companyId;

    /**
     * 店铺id
     */
    @NotNull(message = "请选择收银店铺")
    private Integer shopId;
    /**
     * 会员卡信息
     */
    @NotNull(message = "会员卡信息空")
    private VipCardAO vipCard;
    /**
     * 商品列表
     */
    @NotNull(message = "请添加商品")
    private List<GoodsAO> goodsList;

    /**
     * 操作员
     */
    private String operator;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 店铺名称
     */
    private String shopName;

    private Integer kaidanUserId;

}
