package com.glkj.vipsystem.modules.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.glkj.vipsystem.common.utils.PageUtils;
import com.glkj.vipsystem.common.utils.Query;
import com.glkj.vipsystem.common.utils.R;
import com.glkj.vipsystem.common.validator.ValidatorUtils;
import com.glkj.vipsystem.entity.gen.Notice;
import com.glkj.vipsystem.service.INoticeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 系统通知控制器
 *
 * @author LiMuchan
 */
@RestController
@RequestMapping("/sys/notice")
public class NoticeController {

    @Resource
    private INoticeService noticeService;

    /**
     * 列表
     */
    @GetMapping(value = "/list")
//    @RequiresPermissions({"sys:notice:all"})
    public R list(@RequestParam Map<String, Object> params) {
        String title = (String) params.get("title");
        String status = (String) params.get("status");
        Page<Notice> page = noticeService.selectPage(
                new Query<Notice>(params).getPage(),
                new EntityWrapper<Notice>()
                        .orderBy("order_num asc, create_time desc")
                        .eq(StringUtils.isNotBlank(status), "status", status)
                        .like(StringUtils.isNotBlank(title), "title", title)
        );
        return R.ok().put("page", new PageUtils(page));
    }

    /**
     * 详情
     */
    @GetMapping(value = "/detail/{id}")
//    @RequiresPermissions({"sys:notice:all"})
    public R detail(@PathVariable("id") Integer id) {
        return R.ok().put("item", noticeService.selectById(id));
    }

    /**
     * 保存新增
     */
    @PostMapping(value = "/save")
    @RequiresPermissions({"sys:notice:all"})
    public R save(@RequestBody Notice notice) {
        ValidatorUtils.validateEntity(notice);
        notice.setCreateTime(new Date());
        noticeService.insert(notice);
        return R.ok();
    }


    /**
     * 修改
     */
    @PostMapping(value = "/update")
    @RequiresPermissions({"sys:notice:all"})
    public R update(@RequestBody Notice notice) {
        ValidatorUtils.validateEntity(notice);
        noticeService.updateById(notice);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping(value = "/delete")
    @RequiresPermissions({"sys:notice:all"})
    public R delete(@RequestBody Integer[] ids) {
        if (ids != null && ids.length > 0) {
            noticeService.deleteBatchIds(Arrays.asList(ids));
        }
        return R.ok();
    }
}
