package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.glkj.vipsystem.entity.gen.Stock;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 库存 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
public interface StockMapper extends BaseMapper<Stock> {

    List<Stock> selectShopStocks(Pagination page
            , @Param("companyId") Integer companyId
            , @Param("shopId") Integer shopId
            , @Param("goodsName") String goodsName);

}
