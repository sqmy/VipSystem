package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.VipCardCancelRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员卡注销记录 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-02-20
 */
public interface VipCardCancelRecordMapper extends BaseMapper<VipCardCancelRecord> {

}
