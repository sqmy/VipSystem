package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.AreaAO;

/**
 * <p>
 * Area Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface AreaMapper extends BaseMapper<AreaAO> {
}