package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.MemberAO;

/**
 * <p>
 * Member Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface MemberMapper extends BaseMapper<MemberAO> {
}