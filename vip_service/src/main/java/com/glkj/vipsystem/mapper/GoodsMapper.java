package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.GoodsAO;

/**
 * <p>
 * Goods Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface GoodsMapper extends BaseMapper<GoodsAO> {
}