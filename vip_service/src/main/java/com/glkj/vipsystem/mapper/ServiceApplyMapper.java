package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.ServiceApplyAO;

/**
 * <p>
 * ServiceApply Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface ServiceApplyMapper extends BaseMapper<ServiceApplyAO> {
}