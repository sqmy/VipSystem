package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.gen.KaidanUser;

/**
 * <p>
 * KaidanUser Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface KaidanUserMapper extends BaseMapper<KaidanUser> {
}