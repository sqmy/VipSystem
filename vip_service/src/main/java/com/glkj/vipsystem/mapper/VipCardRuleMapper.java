package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.VipCardRuleAO;

/**
 * <p>
 * VipCardRule Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface VipCardRuleMapper extends BaseMapper<VipCardRuleAO> {
}