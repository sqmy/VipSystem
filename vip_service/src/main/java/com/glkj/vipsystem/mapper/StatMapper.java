package com.glkj.vipsystem.mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据统计 mapper
 * </p>
 *
 * @author Limuchan
 */
public interface StatMapper {

    /**
     * 统计销售收入
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    List<Map> statSale(String startTime, String endTime);

    /**
     * 统计订单收入金额
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    List<Map> statIncomeOrder(String startTime, String endTime);

    /**
     * 统计会员开卡及充值续费金额
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    List<Map> statIncomeVip(String startTime, String endTime);

}