package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.VipCardAO;

/**
 * <p>
 * VipCard Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface VipCardMapper extends BaseMapper<VipCardAO> {
}