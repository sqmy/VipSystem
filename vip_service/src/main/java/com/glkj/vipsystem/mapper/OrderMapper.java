package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.OrderAO;

/**
 * <p>
 * Order Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface OrderMapper extends BaseMapper<OrderAO> {
}