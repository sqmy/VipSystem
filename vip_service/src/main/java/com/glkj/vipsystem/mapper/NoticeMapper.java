package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.Notice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统通知 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-16
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
