package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.EmployeeAO;

/**
 * <p>
 * Employee Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface EmployeeMapper extends BaseMapper<EmployeeAO> {
}