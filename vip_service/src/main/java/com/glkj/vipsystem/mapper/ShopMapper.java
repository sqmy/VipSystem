package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.ShopAO;

/**
 * <p>
 * Shop Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface ShopMapper extends BaseMapper<ShopAO> {
}