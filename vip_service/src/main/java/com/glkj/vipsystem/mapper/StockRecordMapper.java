package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.StockRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 库存变动记录 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
public interface StockRecordMapper extends BaseMapper<StockRecord> {

}
