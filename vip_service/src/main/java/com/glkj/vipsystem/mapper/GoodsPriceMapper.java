package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.GoodsPrice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品店铺价格 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-03-12
 */
public interface GoodsPriceMapper extends BaseMapper<GoodsPrice> {

}
