package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.GoodsCatAO;

/**
 * <p>
 * GoodsCat Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface GoodsCatMapper extends BaseMapper<GoodsCatAO> {
}