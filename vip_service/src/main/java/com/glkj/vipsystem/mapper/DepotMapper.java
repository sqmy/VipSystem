package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.Depot;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 仓库 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
public interface DepotMapper extends BaseMapper<Depot> {

}
