package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.SmsCodeAO;

/**
 * <p>
 * SmsCode Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface SmsCodeMapper extends BaseMapper<SmsCodeAO> {
}