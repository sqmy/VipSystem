package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.VipCardRecordAO;

/**
 * <p>
 * VipCardRecord Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface VipCardRecordMapper extends BaseMapper<VipCardRecordAO> {
}