package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.CompanyAO;

/**
 * <p>
 * Company Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface CompanyMapper extends BaseMapper<CompanyAO> {
}