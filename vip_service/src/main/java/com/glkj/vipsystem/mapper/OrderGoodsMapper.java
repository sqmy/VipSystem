package com.glkj.vipsystem.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.glkj.vipsystem.entity.ao.OrderGoodsAO;

/**
 * <p>
 * OrderGoods Mapper 接口
 * </p>
 *
 * @author Limuchan
 */
public interface OrderGoodsMapper extends BaseMapper<OrderGoodsAO> {
}