package com.glkj.vipsystem.mapper;

import com.glkj.vipsystem.entity.gen.StockApplyFormGoods;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 出入、移库单-商品关联关系 Mapper 接口
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
public interface StockApplyFormGoodsMapper extends BaseMapper<StockApplyFormGoods> {

}
