package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-06
 */
@Data
@Accessors(chain = true)
@TableName("t_goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品分类id
     */
    @TableField("cat_id")
    @NotNull(message = "请选择商品分类")
    private Integer catId;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    @NotNull(message = "请选择公司")
    private Integer companyId;
    /**
     * 商品名称
     */
    @TableField("goods_name")
    @NotBlank(message = "商品名称不能为空")
    private String goodsName;
    /**
     * 商品单位
     */
    @TableField("goods_unit")
    @NotBlank(message = "商品单位不能为空")
    private String goodsUnit;
    /**
     * 商品单价
     */
    @TableField("goods_price")
    @NotNull(message = "商品价格不能为空")
    private BigDecimal goodsPrice;
    /**
     * 商品折扣
     */
    @TableField("goods_discount")
    private Integer goodsDiscount;
    /**
     * 可用会员卡
     * {@link com.glkj.vipsystem.common.utils.Constant.VipCardType}
     */
    @TableField("card_type")
    @NotBlank(message = "请选择结账限制")
    private String cardType;


}
