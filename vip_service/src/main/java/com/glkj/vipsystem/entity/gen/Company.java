package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 公司
 * </p>
 *
 * @author LiMuchan
 * @since 2019-08-28
 */
@Data
@Accessors(chain = true)
@TableName("t_company")
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 公司名称
     */
    @TableField("company_name")
    @NotBlank(message = "请填写公司名称", groups = {AddGroup.class, UpdateGroup.class})
    private String companyName;
    /**
     * 公司简称
     */
    @TableField("company_short_name")
    private String companyShortName;
    /**
     * 公司区域code
     */
    @TableField("company_area_code")
    private String companyAreaCode;
    /**
     * 公司区域信息
     */
    @TableField("company_area_name")
    private String companyAreaName;
    /**
     * 公司详细地址
     */
    @TableField("company_address")
    private String companyAddress;
    /**
     * 公司管理员姓名
     */
    @TableField("manager_name")
    @NotBlank(message = "请填写管理员姓名", groups = {AddGroup.class, UpdateGroup.class})
    private String managerName;
    /**
     * 公司管理员电话
     */
    @TableField("manager_mobile")
    @NotBlank(message = "请填写管理员电话", groups = {AddGroup.class, UpdateGroup.class})
    private String managerMobile;
    /**
     * 后台登录账号
     */
    @NotBlank(message = "登录用户名不能为空", groups = AddGroup.class)
    private String username;
    /**
     * 状态  0：禁用   1：正常
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 公司下允许的店铺数量，负数代表无限制
     */
    @TableField("max_shop_num")
    private Integer maxShopNum;

    /**
     * 店铺下最大员工数量：负数代表无限制
     */
    @TableField("max_employee_num")
    private Integer maxEmployeeNum;
}
