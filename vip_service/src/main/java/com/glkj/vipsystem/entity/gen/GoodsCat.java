package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_goods_cat")
public class GoodsCat implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 父级id
     */
    @TableField("parent_id")
    private Integer parentId;
    /**
     * all id
     */
    @TableField("all_id")
    private String allId;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    @NotNull(message = "请选择所属公司")
    private Integer companyId;
    /**
     * 分类名称
     */
    @TableField("cat_name")
    @NotBlank(message = "分类名称不能为空")
    private String catName;


}
