package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 开单人员
 * </p>
 *
 * @author LiMuchan
 * @since 2019-08-30
 */
@Data
@Accessors(chain = true)
@TableName("t_kaidan_user")
@ApiModel(value = "开单人员")
public class KaidanUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "所属公司id")
    @TableField("company_id")
    @NotNull(message = "请选择所属公司", groups = {AddGroup.class})
    private Integer companyId;

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    @NotBlank(message = "姓名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    @ApiModelProperty(value = "电话")
    @TableField("mobile")
    @NotBlank(message = "电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String mobile;


    @TableField(exist = false)
    private String companyName;
}
