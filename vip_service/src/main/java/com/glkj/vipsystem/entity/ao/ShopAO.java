package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.Shop;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - Shop.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_shop")
@ApiModel(value = "店铺")
public final class ShopAO extends Shop{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    /**
     * 所属公司名称
     */
    @ApiModelProperty(value = "公司名称")
    private transient String companyName;

    /**
     * 店铺员工人数
     */
    private transient int employeeNum;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
