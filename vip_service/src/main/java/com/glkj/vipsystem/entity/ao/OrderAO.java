package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.entity.gen.KaidanUser;
import com.glkj.vipsystem.entity.gen.Order;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - Order.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_order")
@ApiModel(value = "收银订单")
public final class OrderAO extends Order{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private KaidanUser kaidanUser;

    public String getVipCardTypeStr(){
        return Constant.VipCardType.valueOf(this.getVipCardType());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
