package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import com.glkj.vipsystem.entity.ao.GoodsAO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 库存
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
@Data
@Accessors(chain = true)
@TableName("t_stock")
public class Stock implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 店铺id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 仓库id
     */
    @TableField("depot_id")
    private Integer depotId;
    /**
     * 商品id
     */
    @TableField("goods_id")
    private Integer goodsId;
    /**
     * 库存
     */
    private Integer num;

    @TableField(exist = false)
    private GoodsAO goods;

    @TableField(exist = false)
    private String shopName;

}
