package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.VipCardRule;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - VipCardRule.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_vip_card_rule")
public final class VipCardRuleAO extends VipCardRule{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}