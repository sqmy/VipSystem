package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 会员卡
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-07
 */
@Data
@Accessors(chain = true)
@TableName("t_vip_card")
@ApiModel(value = "会员卡")
public class VipCard implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "会员卡id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    @TableField("member_id")
    private Integer memberId;
    /**
     * 所属公司id
     */
    @ApiModelProperty(value = "所属公司id")
    @TableField("company_id")
    @NotNull(message = "请选择所属公司")
    private Integer companyId;
    /**
     * 开卡店铺
     */
    @ApiModelProperty(value = "开卡店铺id")
    @TableField("shop_id")
    @NotNull(message = "请选择开卡店铺")
    private Integer shopId;
    /**
     * 会员卡号
     */
    @ApiModelProperty(value = "会员卡号")
    @TableField("card_no")
    private String cardNo;
    /**
     * 会员姓名
     */
    @ApiModelProperty(value = "会员姓名")
    @TableField("member_name")
    @NotBlank(message = "请输入会员姓名")
    private String memberName;
    /**
     * 会员电话
     */
    @ApiModelProperty(value = "会员电话")
    @TableField("member_mobile")
    @NotBlank(message = "请输入会员电话")
    private String memberMobile;

    /**
     * 性别：1-男，2-女
     */
    @ApiModelProperty(value = "性别")
    private String gender;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "生日")
    private String birthday;

    /**
     * 会员卡类型
     */
    @ApiModelProperty(value = "会员卡类型：1-次卡，2-充值卡，3-积分卡")
    @TableField("card_type")
    @NotNull(message = "请选择卡类型")
    private Integer cardType;
    /**
     * 开卡费
     */
    @ApiModelProperty(value = "实收金额")
    @TableField("card_fee")
    @NotNull(message = "请输入实收金额")
    private BigDecimal cardFee;
    /**
     * 卡余额
     */
    @ApiModelProperty(value = "卡余额")
    @TableField("card_balance")
    private BigDecimal cardBalance;
    /**
     * 卡剩余积分
     */
    @ApiModelProperty(value = "卡剩余积分")
    @TableField("card_points")
    private Integer cardPoints;
    /**
     * 卡剩余次数
     */
    @ApiModelProperty(value = "卡剩余次数")
    @TableField("card_times")
    private Integer cardTimes;
    /**
     * 次数卡类型:1-有限次数卡,0-不限次数卡
     */
    @ApiModelProperty(value = "次卡类型：1-有限次数卡，0-不限次数卡")
    @TableField("card_times_type")
    private Integer cardTimesType;
    /**
     * 开卡日期
     */
    @ApiModelProperty(value = "开卡日期")
    @TableField("create_time")
    private Date createTime;
    /**
     * 失效日期
     */
    @ApiModelProperty(value = "有效期限")
    @TableField("expire_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expireTime;
    /**
     * 操作员
     */
    @ApiModelProperty(value = "开卡操作人")
    private String operator;

}
