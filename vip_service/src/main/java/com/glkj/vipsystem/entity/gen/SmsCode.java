package com.glkj.vipsystem.entity.gen;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 短信验证码
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-24
 */
@Data
@Accessors(chain = true)
@TableName("t_sms_code")
public class SmsCode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @TableId(value = "code_key", type = IdType.INPUT)
    private String codeKey;
    /**
     * 验证码
     */
    private String code;
    /**
     * 过期时间
     */
    @TableField("expire_time")
    private Date expireTime;


}
