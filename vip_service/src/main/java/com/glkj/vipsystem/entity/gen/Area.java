package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地区
 * </p>
 *
 * @author LiMuchan
 * @since 2019-10-14
 */
@Data
@Accessors(chain = true)
@TableName("t_area")
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 地区等级
     */
    private Integer level;
    /**
     * 上级id
     */
    @TableField("parent_id")
    private Integer parentId;
    /**
     * 地区allid
     */
    @TableField("area_all_id")
    private String areaAllId;
    /**
     * 地区全称
     */
    @TableField("area_all_name")
    private String areaAllName;
    /**
     * 是否开通救援服务:0-未开通,1-开通
     */
    private Integer status;


}
