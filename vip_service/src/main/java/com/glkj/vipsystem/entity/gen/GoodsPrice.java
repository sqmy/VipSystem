package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.math.RoundingMode;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品店铺价格
 * </p>
 *
 * @author LiMuchan
 * @since 2020-03-12
 */
@Data
@Accessors(chain = true)
@TableName("t_goods_price")
public class GoodsPrice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 店铺id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 商品id
     */
    @TableField("goods_id")
    private Integer goodsId;
    /**
     * 商品价格
     */
    @TableField("goods_price")
    private BigDecimal goodsPrice;
    /**
     * 商品折扣
     */
    @TableField("goods_discount")
    private Integer goodsDiscount;
    /**
     * 店铺名称
     */
    @TableField(exist = false)
    private String shopName;

    /**
     * 获取打折之后的价格
     */
    public BigDecimal getDiscountPrice() {
        if (this.getGoodsPrice() == null || this.getGoodsDiscount() == null) {
            return BigDecimal.ZERO;
        }
        return this.getGoodsPrice().multiply(BigDecimal.valueOf(this.getGoodsDiscount()))
                .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
    }

}
