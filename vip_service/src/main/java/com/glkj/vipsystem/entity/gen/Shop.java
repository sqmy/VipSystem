package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 公司店铺
 * </p>
 *
 * @author LiMuchan
 * @since 2019-08-30
 */
@Data
@Accessors(chain = true)
@TableName("t_shop")
@ApiModel(value = "店铺")
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "店铺id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 所属公司id
     */
    @ApiModelProperty(value = "所属公司id")
    @TableField("company_id")
    @NotNull(message = "请选择所属公司", groups = {AddGroup.class})
    private Integer companyId;
    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    @TableField("shop_name")
    @NotBlank(message = "店铺名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String shopName;
    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    @TableField("shop_address")
    private String shopAddress;
    /**
     * 管理人
     */
    @ApiModelProperty(value = "管理人")
    @TableField("shop_manager")
    private String shopManager;
    /**
     * 店铺联系电话
     */
    @ApiModelProperty(value = "店铺联系电话")
    @TableField("shop_tel")
//    @NotBlank(message = "店铺联系电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String shopTel;
    /**
     * 店铺微信
     */
    @ApiModelProperty(value = "微信")
    @TableField("shop_wx")
//    @NotBlank(message = "店铺联系电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String shopWx;

    @ApiModelProperty(value = "邮箱")
    @TableField("shop_email")
//    @NotBlank(message = "店铺联系电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String shopEmail;
    /**
     * 状态  0：禁用   1：正常
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
}
