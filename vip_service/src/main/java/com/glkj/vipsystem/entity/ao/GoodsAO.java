package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.Goods;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 应用对象 - Goods.
 * <p>
 *
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_goods")
public final class GoodsAO extends Goods {

    /**
     * 默认的序列化 id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商品分类名称
     */
    private transient String catName;
    /**
     * 商品数量-用于表单提交
     */
    private transient int num;

    /**
     * 获取打折之后的价格
     */
    public BigDecimal getDiscountPrice() {
        if (this.getGoodsPrice() == null || this.getGoodsDiscount() == null) {
            return BigDecimal.ZERO;
        }
        return this.getGoodsPrice().multiply(BigDecimal.valueOf(this.getGoodsDiscount()))
                .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}