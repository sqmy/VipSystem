package com.glkj.vipsystem.entity.gen;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.entity.ao.VipCardAO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 会员卡注销记录
 * </p>
 *
 * @author LiMuchan
 * @since 2020-02-20
 */
@Data
@Accessors(chain = true)
@TableName("t_vip_card_cancel_record")
public class VipCardCancelRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    public VipCardCancelRecord() {

    }

    public VipCardCancelRecord(VipCardAO vipCardAO, String operator) {
        this.vipCardId = vipCardAO.getId();
        this.memberId = vipCardAO.getMemberId();
        this.companyId = vipCardAO.getCompanyId();
        this.shopId = vipCardAO.getShopId();
        this.cardNo = vipCardAO.getCardNo();
        this.memberName = vipCardAO.getMemberName();
        this.memberMobile = vipCardAO.getMemberMobile();
        try {
            this.gender = StringUtils.isNotBlank(vipCardAO.getGender())
                    ? Integer.valueOf(vipCardAO.getGender()) : null;
        } catch (Exception e) {

        }
        try {
            this.birthday = StringUtils.isNotBlank(vipCardAO.getBirthday())
                    ? DateUtil.parseDate(vipCardAO.getBirthday()) : null;
        } catch (Exception e) {

        }
        this.cardType = vipCardAO.getCardType();
        this.cardTimesType = vipCardAO.getCardTimesType();
        this.createTime = vipCardAO.getCreateTime();
        this.cancelTime = new Date();
        this.operator = operator;
    }

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 会员卡id
     */
    @TableField("vip_card_id")
    private Integer vipCardId;
    /**
     * 用户id
     */
    @TableField("member_id")
    private Integer memberId;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    private Integer companyId;
    /**
     * 开卡店铺
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 会员卡号
     */
    @TableField("card_no")
    private String cardNo;
    /**
     * 会员姓名
     */
    @TableField("member_name")
    private String memberName;
    /**
     * 会员电话
     */
    @TableField("member_mobile")
    private String memberMobile;
    /**
     * 性别：0-未知,1-男,2-女
     */
    private Integer gender;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 会员卡类型
     */
    @TableField("card_type")
    private Integer cardType;
    /**
     * 次数卡类型:1-有限次数卡,0-不限次数卡
     */
    @TableField("card_times_type")
    private Integer cardTimesType;
    /**
     * 开卡日期
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 注销日期
     */
    @TableField("cancel_time")
    private Date cancelTime;
    /**
     * 操作员
     */
    private String operator;

    /**
     * 会员卡所属公司
     */
    @ApiModelProperty(value = "公司名称")
    private transient String companyName;
    /**
     * 开卡店铺
     */
    @ApiModelProperty(value = "开卡店铺")
    private transient String shopName;

    public String getCardTypeStr() {
        return Constant.VipCardType.valueOf(this.getCardType());
    }
}
