package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.OrderGoods;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 应用对象 - OrderGoods.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_order_goods")
@ApiModel(value = "订单商品")
public final class OrderGoodsAO extends OrderGoods{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    /**
     * 获取打折之后的价格
     */
    public BigDecimal getDiscountPrice() {
        if (this.getGoodsPrice() == null || this.getGoodsDiscount() == null) {
            return BigDecimal.ZERO;
        }
        return this.getGoodsPrice().multiply(BigDecimal.valueOf(this.getGoodsDiscount()))
                .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
