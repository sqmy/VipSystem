package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 会员卡规则
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-18
 */
@Data
@Accessors(chain = true)
@TableName("t_vip_card_rule")
public class VipCardRule implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    @NotNull(message = "请选择公司", groups = {AddGroup.class})
    private Integer companyId;
    /**
     * 1积分抵扣多少元
     */
    @TableField("points_amount")
    @NotBlank(message = "请输入1积分抵扣多少元", groups = {AddGroup.class, UpdateGroup.class})
    private String pointsAmount;
    /**
     * 充值1元赠送多少元
     */
    @TableField("give_amount")
//    @NotBlank(message = "请输入充值1元赠送多少元", groups = {AddGroup.class, UpdateGroup.class})
    private String giveAmount;


}
