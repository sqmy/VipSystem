package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 仓库
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
@Data
@Accessors(chain = true)
@TableName("t_depot")
public class Depot implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 店铺id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 仓库名称
     */
    private String name;

    /**
     * 库存是否已初始化完成：1-正在初始化，2-已完成
     */
    @TableField("init_complete")
    private Integer initComplete;

}
