package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.GoodsCat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - GoodsCat.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_goods_cat")
public final class GoodsCatAO extends GoodsCat{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}