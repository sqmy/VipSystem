package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 会员卡变更记录
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-09
 */
@Data
@Accessors(chain = true)
@TableName("t_vip_card_record")
public class VipCardRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 会员卡id
     */
    @TableField("vip_card_id")
    private Integer vipCardId;
    /**
     * 用户id
     */
    @TableField("member_id")
    private Integer memberId;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    private Integer companyId;
    /**
     * 操作店铺
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 会员卡号
     */
    @TableField("card_no")
    private String cardNo;
    /**
     * 用户姓名
     */
    @TableField("member_name")
    private String memberName;
    /**
     * 用户电话
     */
    @TableField("member_mobile")
    private String memberMobile;
    /**
     * 会员卡类型
     */
    @TableField("card_type")
    private Integer cardType;
    /**
     * 变更类型：1-后台调整,2-消费,3-新开
     */
    @TableField("change_type")
    private Integer changeType;
    /**
     * 变更费用/实收金额
     */
    @TableField("change_fee")
    private BigDecimal changeFee;
    /**
     * 余额变更
     */
    @TableField("balance_change")
    private BigDecimal balanceChange;
    /**
     * 次数变更
     */
    @TableField("times_change")
    private Integer timesChange;
    /**
     * 次数卡类型变化:1-不限次变更为有限次,0-有限次变更为不限次
     */
    @TableField("times_change_type")
    private Integer timesChangeType;
    /**
     * 积分变更
     */
    @TableField("points_change")
    private Integer pointsChange;
    /**
     * 有效期变更：原有效期-现有效期
     */
    @TableField("expire_time_change")
    private String expireTimeChange;
    /**
     * 变更说明
     */
    @TableField("change_desc")
    private String changeDesc;
    /**
     * 变更时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 操作员
     */
    private String operator;


}
