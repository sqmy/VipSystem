package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 店铺员工
 * </p>
 *
 * @author LiMuchan
 * @since 2019-10-14
 */
@Data
@Accessors(chain = true)
@TableName("t_employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 所属店铺id
     */
    @TableField("shop_id")
    @NotNull(message = "请选择所属店铺", groups = {AddGroup.class, UpdateGroup.class})
    private Integer shopId;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    @NotNull(message = "请选择所属公司", groups = {AddGroup.class, UpdateGroup.class})
    private Integer companyId;
    /**
     * 员工姓名
     */
    @TableField("employee_name")
    @NotBlank(message = "请填写员工姓名", groups = {AddGroup.class, UpdateGroup.class})
    private String employeeName;
    /**
     * 员工电话
     */
    @TableField("employee_mobile")
    @NotBlank(message = "请填写员工电话", groups = {AddGroup.class, UpdateGroup.class})
    private String employeeMobile;
    /**
     * 后台登录账号
     */
    @NotBlank(message = "登录用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String username;
    /**
     * 状态  0：禁用   1：正常
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 性别：0-未知,1-男,2-女
     */
    private Integer gender;
    /**
     * 生日
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    /**
     * 身份证号
     */
    @TableField("id_number")
    private String idNumber;
    /**
     * 住址
     */
    private String address;
    /**
     * 紧急联系人
     */
    @TableField("emergency_contact")
    private String emergencyContact;
    /**
     * 紧急联系人电话
     */
    @TableField("emergency_contact_phone")
    private String emergencyContactPhone;
    /**
     * 员工等级
     */
    private String level;


}
