package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.entity.gen.VipCard;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - VipCard.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_vip_card")
@ApiModel(value = "会员卡")
public final class VipCardAO extends VipCard{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    /**
     * 会员卡所属公司
     */
    @ApiModelProperty(value = "公司名称")
    private transient String companyName;
    /**
     * 开卡店铺
     */
    @ApiModelProperty(value = "开卡店铺")
    private transient String shopName;

    public String getCardTypeStr(){
        return Constant.VipCardType.valueOf(this.getCardType());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
