package com.glkj.vipsystem.entity.form;

import com.glkj.vipsystem.entity.gen.GoodsPrice;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 店铺价格设置表单
 */
@Getter
@Setter
public class GoodsPriceForm {

    private List<GoodsPrice> priceList;

}
