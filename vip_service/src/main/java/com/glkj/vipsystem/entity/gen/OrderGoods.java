package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收银订单商品信息表
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-11
 */
@Data
@Accessors(chain = true)
@TableName("t_order_goods")
@ApiModel(value = "收银订单商品")
public class OrderGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    @TableField("order_id")
    private Long orderId;
    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id")
    @TableField("goods_id")
    private Integer goodsId;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @TableField("goods_name")
    private String goodsName;
    /**
     * 商品单位
     */
    @ApiModelProperty(value = "商品单位")
    @TableField("goods_unit")
    private String goodsUnit;
    /**
     * 商品单价
     */
    @ApiModelProperty(value = "商品单价")
    @TableField("goods_price")
    private BigDecimal goodsPrice;
    /**
     * 商品折扣
     */
    @TableField("goods_discount")
    private Integer goodsDiscount;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    @TableField("goods_num")
    private Integer goodsNum;
    /**
     * 可用会员卡类型
     */
    @ApiModelProperty(value = "可用会员卡类型")
    @TableField("card_type")
    private String cardType;
    /**
     * 商品分类id
     */
    @ApiModelProperty(value = "商品分类id")
    @TableField("goods_cat_id")
    private Integer goodsCatId;
    /**
     * 商品分类名称
     */
    @ApiModelProperty(value = "商品分类名称")
    @TableField("goods_cat_name")
    private String goodsCatName;


}
