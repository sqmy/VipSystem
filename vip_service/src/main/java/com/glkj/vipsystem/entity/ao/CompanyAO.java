package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.common.validator.group.AddGroup;
import com.glkj.vipsystem.entity.gen.Company;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;

/**
 * 应用对象 - Company.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_company")
public final class CompanyAO extends Company{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;
    /**
     * 公司店铺数量
     */
    private transient int shopNum;
    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空",groups = AddGroup.class)
    private transient String password;
    /**
     * 确认密码
     */
    @NotBlank(message = "确认密码不能为空",groups = AddGroup.class)
    private transient String confirmPassword;

    /**
     * 验证码 找回密码用
     */
    private transient String code;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
