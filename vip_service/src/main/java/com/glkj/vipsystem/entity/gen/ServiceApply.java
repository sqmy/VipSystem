package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 购买服务申请表
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-25
 */
@Data
@Accessors(chain = true)
@TableName("t_service_apply")
@ApiModel(value = "服务购买申请")
public class ServiceApply implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 购买的服务
     */
    @NotBlank(message = "请选择要购买的服务")
    @ApiModelProperty(value = "购买的服务")
    private String type;
    /**
     * 联系人
     */
    @NotBlank(message = "请填写姓名")
    @ApiModelProperty(value = "姓名")
    @TableField("contact_name")
    private String contactName;
    /**
     * 联系电话
     */
    @NotBlank(message = "请填写电话")
    @ApiModelProperty(value = "电话")
    @TableField("contact_mobile")
    private String contactMobile;
    /**
     * 地址信息
     */
    @NotBlank(message = "请填填写地址信息")
    @ApiModelProperty(value = "地址信息")
    private String address;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
