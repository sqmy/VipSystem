package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.entity.gen.VipCardRecord;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - VipCardRecord.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_vip_card_record")
public final class VipCardRecordAO extends VipCardRecord{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    /**
     * 公司名称
     */
    private transient String companyName;

    /**
     * 店铺名称
     */
    private transient String shopName;

    public String getCardTypeStr(){
        return Constant.VipCardType.valueOf(this.getCardType());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
