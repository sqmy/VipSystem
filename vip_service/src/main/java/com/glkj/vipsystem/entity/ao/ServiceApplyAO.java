package com.glkj.vipsystem.entity.ao;

import com.baomidou.mybatisplus.annotations.TableName;
import com.glkj.vipsystem.entity.gen.ServiceApply;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 应用对象 - ServiceApply.
 * <p>
 * @author Limuchan
 */
@Getter
@Setter
@TableName("t_service_apply")
@ApiModel(value = "购买服务申请表单")
public final class ServiceApplyAO extends ServiceApply{

    /**
    * 默认的序列化 id.
    */
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
