package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收银订单
 * </p>
 *
 * @author LiMuchan
 * @since 2019-09-11
 */
@Data
@Accessors(chain = true)
@TableName("t_order")
@ApiModel(value = "收银订单")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "订单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String sn;
    /**
     * 订单金额
     */
    @ApiModelProperty(value = "订单总金额")
    @TableField("order_amount")
    private BigDecimal orderAmount;
    /**
     * 现金支付金额
     */
    @ApiModelProperty(value = "实收金额")
    @TableField("pay_amount")
    private BigDecimal payAmount;
    /**
     * 充值卡余额抵扣金额
     */
    @ApiModelProperty(value = "余额抵扣金额")
    @TableField("balance_amount")
    private BigDecimal balanceAmount;
    /**
     * 次数消耗
     */
    @ApiModelProperty(value = "次数消耗")
    @TableField("times_cost")
    private Integer timesCost;
    /**
     * 积分消耗
     */
    @ApiModelProperty(value = "积分消耗")
    @TableField("points_cost")
    private Integer pointsCost;
    /**
     * 积分抵扣金额
     */
    @ApiModelProperty(value = "积分抵扣金额")
    @TableField("points_amount")
    private BigDecimal pointsAmount;
    /**
     * 订单赠送积分
     */
    @ApiModelProperty(value = "赠送积分")
    @TableField("points_give")
    private Integer pointsGive;
    /**
     * 会员卡id
     */
    @ApiModelProperty(value = "会员卡id")
    @TableField("vip_card_id")
    private Integer vipCardId;
    /**
     * 会员卡号
     */
    @ApiModelProperty(value = "会员卡号")
    @TableField("vip_card_no")
    private String vipCardNo;
    /**
     * 会员卡类型
     */
    @ApiModelProperty(value = "会员卡类型")
    @TableField("vip_card_type")
    private Integer vipCardType;
    /**
     * 会员id
     */
    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Integer memberId;
    /**
     * 会员姓名
     */
    @ApiModelProperty(value = "会员姓名")
    @TableField("member_name")
    private String memberName;
    /**
     * 会员电话
     */
    @ApiModelProperty(value = "会员电话")
    @TableField("member_mobile")
    private String memberMobile;
    /**
     * 所属公司id
     */
    @ApiModelProperty(value = "所属公司id")
    @TableField("company_id")
    private Integer companyId;
    /**
     * 所属公司名称
     */
    @ApiModelProperty(value = "所属公司名称")
    @TableField("company_name")
    private String companyName;
    /**
     * 收银店铺id
     */
    @ApiModelProperty(value = "收银店铺id")
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 收银店铺名称
     */
    @ApiModelProperty(value = "收银店铺名称")
    @TableField("shop_name")
    private String shopName;
    /**
     * 操作员
     */
    @ApiModelProperty(value = "操作员")
    private String operator;
    /**
     * 收银时间
     */
    @ApiModelProperty(value = "收银时间")
    @TableField("create_time")
    private Date createTime;
    /**
     * 开单员id
     */
    @ApiModelProperty(value = "开单员id")
    @TableField("kaidan_user_id")
    private Integer kaidanUserId;

}
