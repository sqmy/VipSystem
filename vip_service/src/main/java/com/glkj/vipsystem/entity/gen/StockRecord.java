package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.enums.StockRecordType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 库存变动记录
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
@Data
@Accessors(chain = true)
@TableName("t_stock_record")
public class StockRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 店铺id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 仓库id
     */
    @TableField("depot_id")
    private Integer depotId;
    /**
     * 商品id
     */
    @TableField("goods_id")
    private Integer goodsId;
    /**
     * 数量
     */
    private Integer num;
    /**
     * 类型：出库，入库，移库入库，移库出库，销售出库
     */
    private Integer type;
    /**
     * 出入、移库单id，只有库存操作才有
     */
    @TableField("form_id")
    private Integer formId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    @TableField(exist = false)
    private String shopName;

    @TableField(exist = false)
    private String goodsName;

    public String getTypeStr() {
        return StockRecordType.valueOf(this.type);
    }

}
