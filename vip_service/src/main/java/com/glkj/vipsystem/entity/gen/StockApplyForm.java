package com.glkj.vipsystem.entity.gen;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.glkj.vipsystem.common.enums.StockApplyFormType;
import com.glkj.vipsystem.entity.ao.GoodsAO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 出入、移库单
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
@Data
@Accessors(chain = true)
@TableName("t_stock_apply_form")
public class StockApplyForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 所属公司id
     */
    @TableField("company_id")
    private Integer companyId;
    /**
     * 出货店铺id
     */
    @TableField("out_shop_id")
    private Integer outShopId;
    /**
     * 入货店铺id
     */
    @TableField("in_shop_id")
    private Integer inShopId;
    /**
     * 入货仓库id
     */
    @TableField("in_depot_id")
    private Integer inDepotId;
    /**
     * 出货仓库id
     */
    @TableField("out_depot_id")
    private Integer outDepotId;
    /**
     * 类型：出库单；入库单；移库单
     */
    private Integer type;
    /**
     * 申请时间
     */
    @TableField("create_time")
    private Date createTime;

    @TableField("action_user_id")
    private Long actionUserId;
    /**
     * 审核时间
     */
    @TableField("verify_time")
    private Date verifyTime;

    @TableField("verify_user_id")
    private Long verifyUserId;
    /**
     * 状态：1-申请中/待审核,2-成功, 3-审核失败
     */
    private Integer status;

    @TableField(exist = false)
    private String inShopName;

    @TableField(exist = false)
    private String outShopName;

    @TableField(exist = false)
    private String actionUsername;

    @TableField(exist = false)
    private String verifyUsername;

    @TableField(exist = false)
    private List<GoodsAO> goodsList;

    public String getTypeStr() {
        return StockApplyFormType.valueOf(this.type);
    }

    public String getStatusStr() {
        if (this.status == null) return "";
        switch (this.status) {
            case 1:
                return "待审核";
            case 2:
                return "成功";
            default:
                return "";
        }
    }

}
