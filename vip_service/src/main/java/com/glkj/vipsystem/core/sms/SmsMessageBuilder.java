//package com.glkj.vipsystem.core.sms;
//
//import com.glkj.yunguanjia.config.properties.SystemProperties;
//import com.glkj.yunguanjia.core.sms.aliyun.AliyunSmsTemplate;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.Executors;
//
///**
// * 短信发送工具
// */
//@Slf4j
//@Component
//public class SmsMessageBuilder {
//
//    @Resource
//    private SystemProperties systemProperties;
//
//    /**
//     * 用户发起服务时，给管理人员发短信通知
//     *
//     * @param serviceType 服务类型
//     */
//    public void sendServiceMessageToManager(final String serviceType) {
//        Executors.newSingleThreadExecutor().execute(() -> {
//            // 添加模板内容
//            Map<String, Object> param = new HashMap<>();
//            // 模板code
//            param.put("template_code", AliyunSmsTemplate.ServiceMessage.getCode());
//            param.put("serviceType", serviceType);
//            ISmsSendEvent smsPlugin = SmsPluginFactory.getSmsPlugin();
//            smsPlugin.onSend(systemProperties.getServiceMessageMobiles(), "", param);
//        });
//    }
//
//}
