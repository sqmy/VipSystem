package com.glkj.vipsystem.core.sms;

/**
 * 短信插件基类
 *
 * @author LiMuchan
 */
public abstract class AbstractSmsPlugin {


    /**
     * 为短信插件定义唯一的id
     *
     * @return id
     */
    public abstract String getId();


    /**
     * 定义插件名称
     *
     * @return 名称
     */
    public abstract String getName();

}
