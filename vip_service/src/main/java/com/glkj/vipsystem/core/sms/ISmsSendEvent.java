package com.glkj.vipsystem.core.sms;

import java.util.Map;

/**
 * 短信发送事件
 *
 * @author LiMuchan
 */
public interface ISmsSendEvent {

    /**
     * 发送短信事件
     *
     * @param phone   手机号
     * @param content 发送内容
     * @param param   其它参数
     * @return 是否发送成功
     */
    public boolean onSend(String phone, String content, Map param);
}
