package com.glkj.vipsystem.core.sms;

import com.glkj.vipsystem.common.utils.SpringContextUtils;

/**
 * 短信插件工厂
 */
public class SmsPluginFactory {

    /**
     * 获取短信插件
     */
    public static ISmsSendEvent getSmsPlugin() {
        return SpringContextUtils.getBean("AliyunSmsPlugin");
    }

}
