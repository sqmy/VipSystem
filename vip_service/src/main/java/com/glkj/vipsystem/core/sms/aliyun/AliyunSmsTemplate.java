package com.glkj.vipsystem.core.sms.aliyun;

/**
 * 阿里云短信模板枚举类
 */
public enum AliyunSmsTemplate {

    // 验证码
    Bind("SMS_175365054",new String[]{"code"}),
    NewPassword("SMS_175365053",new String[]{"code"});

    AliyunSmsTemplate(String code, String[] params) {
        this.code = code;
        this.params = params;
    }

    private String code;

    private String[] params;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String[] getParams() {
        return params;
    }

    public void setParams(String[] params) {
        this.params = params;
    }

    public static String[] getParams(String code) {
        if (code == null) {
            return null;
        } else {
            for (AliyunSmsTemplate ms : AliyunSmsTemplate.values()) {
                if (code.equals(ms.getCode())) {
                    return ms.getParams();
                }
            }
            return null;
        }
    }
}
