package com.glkj.vipsystem.common.enums;

import lombok.Getter;

/**
 * 出入、移库单类型
 */
@Getter
public enum StockApplyFormType {

    Chuku(1, "出库单"),
    Ruku(2, "入库单"),
    Yiku(3, "移库单");

    StockApplyFormType(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public static String valueOf(Integer type) {
        if (type == null) {
            return "";
        }
        StockApplyFormType[] values = StockApplyFormType.values();
        for (StockApplyFormType value : values) {
            if (type == value.type) return value.msg;
        }
        return "";
    }

    private int type;

    private String msg;

}
