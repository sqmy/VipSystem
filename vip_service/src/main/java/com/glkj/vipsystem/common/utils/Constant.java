package com.glkj.vipsystem.common.utils;

/**
 * 常量
 *
 * @author LiMuchan
 * @date 2016年11月15日 下午1:23:52
 */
public class Constant {
    /**
     * 超级管理员ID
     */
    public static final int SUPER_ADMIN = 1;

    /**
     * 固定角色id
     */
    public enum Role {

        /**
         * 公司管理员
         */
        COMPANY(1L),
        /**
         * 员工角色
         */
        EMPLOYEE(2L);

        private long value;

        Role(long value) {
            this.value = value;
        }

        public long getValue() {
            return value;
        }
    }

    /**
     * 菜单类型
     *
     * @author LiMuchan
     * @date 2016年11月15日 下午1:24:29
     */
    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 定时任务状态
     *
     * @author LiMuchan
     * @date 2016年12月3日 上午12:07:22
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
        NORMAL(0),
        /**
         * 暂停
         */
        PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 会员卡类型
     */
    public enum VipCardType {
        CIKA(1, "次卡"),
        CHONGZHIKA(2, "充值卡"),
        JIFENKA(3, "积分卡"),
        SANKE(4, "散客");

        private int value;

        private String message;

        VipCardType(int value, String message) {
            this.value = value;
            this.message = message;
        }

        public static String valueOf(int value) {
            String message = "";
            for (VipCardType vipCardType : VipCardType.values()) {
                if (value == vipCardType.value) {
                    message = vipCardType.message;
                    break;
                }
            }
            return message;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 会员卡变更记录方式
     */
    public enum VipCardRecordChangeType {
        SYSTEM(1, "后台调整"),
        COST(2, "消费"),
        NEW(3, "新开");

        private int value;

        private String message;

        VipCardRecordChangeType(int value, String message) {
            this.value = value;
            this.message = message;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 验证码类型
     */
    public enum SmsCodeType {
        /**
         * 注册
         */
        MemberBind("Sms_Member_Bind_", 1),
        /**
         * 后台找回密码
         */
        SysFindPassword("Sms_Sys_Find_Password_", 2);
        private String key;

        private int type;

        SmsCodeType(String key, int type) {
            this.key = key;
            this.type = type;
        }

        public String key() {
            return key;
        }

        public static SmsCodeType typeOf(Integer type) {
            if (type == null) {
                return null;
            } else {
                for (SmsCodeType ms : SmsCodeType.values()) {
                    if (ms.type == type) {
                        return ms;
                    }
                }
                return null;
            }
        }
    }

}
