package com.glkj.vipsystem.common.enums;

import lombok.Getter;

/**
 * 库存变动类型
 */
@Getter
public enum StockRecordType {

    Chuku(1, "出库"),
    Ruku(2, "入库"),
    YikuChuku(3, "移库出库"),
    YikuRuku(4, "移库入库"),
    XiaoShou(5, "销售出库");

    StockRecordType(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public static String valueOf(Integer type) {
        if (type == null) {
            return "";
        }
        StockRecordType[] values = StockRecordType.values();
        for (StockRecordType value : values) {
            if (type == value.type) return value.msg;
        }
        return "";
    }

    private int type;

    private String msg;

}
