package com.glkj.vipsystem.common.validator;

import com.glkj.vipsystem.common.exception.BizException;
import org.apache.commons.lang.StringUtils;

/**
 * 数据校验
 * @author LiMuchan
 *
 * @date 2017-03-23 15:50
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new BizException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new BizException(message);
        }
    }
}
