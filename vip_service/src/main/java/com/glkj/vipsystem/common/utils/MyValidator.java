package com.glkj.vipsystem.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证工具
 */
public class MyValidator {

    private final static String MoneyFormat = "^(\\d+(?:\\.\\d{1,2})?)$";
    /**
     * 验证经度
     */
    private static final String V_LONGITUDE = "^-?[0-9]{1,3}(\\.[0-9]*)?$";
    /**
     * 验证纬度
     */
    private static final String V_LATITUDE = "^-?[0-9]{1,2}(\\.[0-9]*)?$";

    public static boolean isMoney(String money){
        return match(MoneyFormat,money);
    }

    /**
     * 验证是不是是经度
     *
     * @param value 要验证的字符串
     * @return 如果是符合格式的字符串, 返回 <b>true </b>,否则为 <b>false </b>
     */
    public static boolean Longitude(String value) {
        return match(V_LONGITUDE, value);
    }

    /**
     * 验证是不是是纬度
     *
     * @param value 要验证的字符串
     * @return 如果是符合格式的字符串, 返回 <b>true </b>,否则为 <b>false </b>
     */
    public static boolean Latitude(String value) {
        return match(V_LATITUDE, value);
    }

    /**
     * @param regex 正则表达式字符串
     * @param str   要匹配的字符串
     * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
     */
    private static boolean match(String regex, String str) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }



}
