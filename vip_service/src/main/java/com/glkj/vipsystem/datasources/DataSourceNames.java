package com.glkj.vipsystem.datasources;

/**
 * 增加多数据源，在此配置
 *
 * @author LiMuchan
 *
 * @date 2017/8/18 23:46
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";

}
