package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.VipCardRuleAO;

/**
 * <p>
 * VipCardRule 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-18 13:54:15
 */
public interface IVipCardRuleService extends IService<VipCardRuleAO> {
}