package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.entity.ao.ShopAO;
import com.glkj.vipsystem.entity.gen.Depot;
import com.glkj.vipsystem.entity.gen.Stock;
import com.glkj.vipsystem.mapper.StockMapper;
import com.glkj.vipsystem.service.IDepotService;
import com.glkj.vipsystem.service.IShopService;
import com.glkj.vipsystem.service.IStockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 库存 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, Stock> implements IStockService {

    @Resource
    private IDepotService depotService;
    @Resource
    private IShopService shopService;

    @Override
    public Page<Stock> selectShopStocks(Page<Stock> page, Integer companyId, Integer shopId, String goodsName) {
        page.setRecords(baseMapper.selectShopStocks(page, companyId, shopId, goodsName));
        return page;
    }

    /**
     * 新增库存关联信息
     *
     * @param shopId  店铺id
     * @param goodsId 商品信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addStock(Integer shopId, Integer goodsId) {
        if (shopId == null || goodsId == null) {
            throw new BizException("新增库存关联失败，参数错误");
        }
        // 初始化库存
        ShopAO shopAO = shopService.selectById(shopId);
        if (shopAO == null) {
            throw new BizException("店铺不存在");
        }
        // 查看是否已建立仓库
        // 建立仓库
        // 若店铺已建立仓库
        Depot selectOne = depotService.selectOne(
                new EntityWrapper<Depot>()
                        .eq("shop_id", shopId)
                        .last("limit 1")
        );
        Depot depot = new Depot();
        if (selectOne != null) {
            depot.setId(selectOne.getId());
        }
        depot.setShopId(shopId);
        depot.setInitComplete(2); // 完成
        depotService.insertOrUpdate(depot);

        // 记录库存
        Stock stock = new Stock();
        int count = this.selectCount(
                new EntityWrapper<Stock>()
                        .eq("shop_id", shopId)
                        .eq("goods_id", goodsId)
                        .eq("depot_id", depot.getId())
        );
        if (count > 0) {
            return;
        }
        stock.setShopId(shopId);
        stock.setDepotId(depot.getId());
        stock.setGoodsId(goodsId);
        stock.setNum(0);
        this.insert(stock);
    }

    @Override
    public Stock selectOne(Integer shopId, Integer depotId, Integer goodsId) {
        if (shopId == null || depotId == null || goodsId == null) {
            return null;
        }
        return this.selectOne(
                new EntityWrapper<Stock>()
                        .eq("shop_id", shopId)
                        .eq("depot_id", depotId)
                        .eq("goods_id", goodsId)
                        .last("limit 1")
        );
    }
}
