package com.glkj.vipsystem.service;

import com.glkj.vipsystem.entity.gen.StockApplyFormGoods;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 出入、移库单-商品关联关系 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
public interface IStockApplyFormGoodsService extends IService<StockApplyFormGoods> {

}
