package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.common.utils.Constant;
import com.glkj.vipsystem.core.sms.aliyun.AliyunSmsTemplate;
import com.glkj.vipsystem.entity.ao.SmsCodeAO;

/**
 * <p>
 * SmsCode 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-24 10:58:47
 */
public interface ISmsCodeService extends IService<SmsCodeAO> {

    /**
     * 验证码效验
     *
     * @param type   验证码类型
     * @param mobile 手机号
     * @param code   验证码
     * @return true：成功  false：失败
     */
    boolean validate(Constant.SmsCodeType type, String mobile, String code);

    /**
     * 发送验证码
     *
     * @param template 短信模板
     * @param type     验证码类型
     * @param mobile   手机号
     * @param seconds  有效时间(s)
     */
    void send(AliyunSmsTemplate template, Constant.SmsCodeType type, String mobile, int seconds);

}
