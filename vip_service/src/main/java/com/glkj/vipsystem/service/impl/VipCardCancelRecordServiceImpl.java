package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.entity.gen.VipCardCancelRecord;
import com.glkj.vipsystem.mapper.VipCardCancelRecordMapper;
import com.glkj.vipsystem.service.IVipCardCancelRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员卡注销记录 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-02-20
 */
@Service
public class VipCardCancelRecordServiceImpl extends ServiceImpl<VipCardCancelRecordMapper, VipCardCancelRecord> implements IVipCardCancelRecordService {

}
