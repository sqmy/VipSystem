package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.entity.ao.GoodsAO;
import com.glkj.vipsystem.entity.ao.ShopAO;
import com.glkj.vipsystem.entity.gen.Depot;
import com.glkj.vipsystem.entity.gen.Stock;
import com.glkj.vipsystem.mapper.DepotMapper;
import com.glkj.vipsystem.service.IDepotService;
import com.glkj.vipsystem.service.IGoodsService;
import com.glkj.vipsystem.service.IShopService;
import com.glkj.vipsystem.service.IStockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 仓库 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
@Service
public class DepotServiceImpl extends ServiceImpl<DepotMapper, Depot> implements IDepotService {

    @Resource
    private IGoodsService goodsService;
    @Resource
    private IShopService shopService;
    @Resource
    private IStockService stockService;

    /**
     * 初始化仓库 库存
     *
     * @param shopId 店铺id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Depot initStock(Integer shopId) {
        if (shopId == null) {
            throw new BizException("店铺id空");
        }
        int count = this.selectCount(new EntityWrapper<Depot>().eq("shop_id", shopId));
        if (count > 0) {
            throw new BizException("仓库正在初始化或已初始化完成");
        }
        // 初始化库存
        ShopAO shopAO = shopService.selectById(shopId);
        if (shopAO == null) {
            throw new BizException("店铺不存在");
        }
        // 建立仓库
        // 若店铺已建立仓库
        Depot selectOne = this.selectOne(
                new EntityWrapper<Depot>()
                        .eq("shop_id", shopId)
                        .last("limit 1")
        );
        Depot depot = new Depot();
        if (selectOne != null) {
            depot.setId(selectOne.getId());
        }
        depot.setShopId(shopId);
        depot.setInitComplete(1); // 正在初始化
        this.insertOrUpdate(depot);

        List<GoodsAO> goodsList = goodsService.selectList(
                new EntityWrapper<GoodsAO>()
                        .eq("company_id", shopAO.getCompanyId())
        );
        if (!CollectionUtils.isEmpty(goodsList)) {
            List<Integer> goodsIds = goodsList.stream().map(GoodsAO::getId).collect(Collectors.toList());
            List<Stock> stocks = new ArrayList<>();
            for (Integer goodsId : goodsIds) {
                Stock stock = new Stock();
                stock.setShopId(shopId);
                stock.setDepotId(depot.getId());
                stock.setGoodsId(goodsId);
                stock.setNum(0);
                stocks.add(stock);
            }
            stockService.insertBatch(stocks);
        }

        // 更新仓库为已初始化完成
        depot.setInitComplete(2);
        this.updateById(depot);

        return depot;
    }
}
