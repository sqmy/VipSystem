package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.EmployeeAO;

import java.util.List;

/**
 * <p>
 * Employee 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-03 11:17:36
 */
public interface IEmployeeService extends IService<EmployeeAO> {

    EmployeeAO selectByUsername(String username);

    /**
     * 新增员工
     *
     * @param employee 员工信息
     */
    void save(EmployeeAO employee, Long createUserId);

    /**
     * 修改员工信息
     *
     * @param employee 员工信息
     */
    void update(EmployeeAO employee);

    /**
     * 批量删除
     *
     * @param ids 要删除的id
     */
    void deleteBatch(List<Integer> ids);

}