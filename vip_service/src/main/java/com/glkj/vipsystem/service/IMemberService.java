package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.MemberAO;
import com.glkj.vipsystem.modules.app.form.BindForm;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Member 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-07 09:39:01
 */
public interface IMemberService extends IService<MemberAO> {

    default MemberAO selectByMobile(String mobile) {
        if (StringUtils.isBlank(mobile)) {
            return null;
        }
        return this.selectOne(
                new EntityWrapper<MemberAO>()
                        .eq("mobile", mobile)
        );
    }

    default MemberAO selectByOpenid(String openid) {
        if (StringUtils.isBlank(openid)) {
            return null;
        }
        return this.selectOne(
                new EntityWrapper<MemberAO>()
                        .eq("openid", openid)
        );
    }

    /**
     * 绑定手机号
     *
     * @param form   表单信息
     * @param openid openid
     * @return 用户id
     */
    Integer bind(BindForm form, String openid);

}
