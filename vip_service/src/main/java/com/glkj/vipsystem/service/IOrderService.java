package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.OrderAO;
import com.glkj.vipsystem.modules.cash.form.CashForm;

/**
 * <p>
 * Order 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-11 14:56:35
 */
public interface IOrderService extends IService<OrderAO> {

    /**
     * 新增收银订单
     * @param cashForm 收银订单信息
     */
    void save(CashForm cashForm);

}