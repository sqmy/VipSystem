package com.glkj.vipsystem.service;

import com.glkj.vipsystem.entity.gen.GoodsPrice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品店铺价格 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-03-12
 */
public interface IGoodsPriceService extends IService<GoodsPrice> {

}
