package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.glkj.vipsystem.entity.ao.OrderGoodsAO;
import com.glkj.vipsystem.mapper.OrderGoodsMapper;
import com.glkj.vipsystem.service.IOrderGoodsService;

/**
 * <p>
 * OrderGoods 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-11 14:56:35
 */
@Service
public class OrderGoodsService extends ServiceImpl<OrderGoodsMapper, OrderGoodsAO> implements IOrderGoodsService {
}