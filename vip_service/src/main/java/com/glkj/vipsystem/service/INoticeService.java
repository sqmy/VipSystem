package com.glkj.vipsystem.service;

import com.glkj.vipsystem.entity.gen.Notice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统通知 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-16
 */
public interface INoticeService extends IService<Notice> {

}
