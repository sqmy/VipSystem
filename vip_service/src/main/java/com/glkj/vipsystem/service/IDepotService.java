package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.gen.Depot;
import com.glkj.vipsystem.entity.gen.Stock;

/**
 * <p>
 * 仓库 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
public interface IDepotService extends IService<Depot> {

    /**
     * 初始化仓库 库存
     *
     * @param shopId 店铺id
     */
    Depot initStock(Integer shopId);



}
