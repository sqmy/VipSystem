package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.glkj.vipsystem.entity.ao.VipCardRecordAO;
import com.glkj.vipsystem.mapper.VipCardRecordMapper;
import com.glkj.vipsystem.service.IVipCardRecordService;

/**
 * <p>
 * VipCardRecord 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-09 16:43:05
 */
@Service
public class VipCardRecordService extends ServiceImpl<VipCardRecordMapper, VipCardRecordAO> implements IVipCardRecordService {
}