package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.glkj.vipsystem.entity.ao.VipCardRuleAO;
import com.glkj.vipsystem.mapper.VipCardRuleMapper;
import com.glkj.vipsystem.service.IVipCardRuleService;

/**
 * <p>
 * VipCardRule 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-18 13:54:15
 */
@Service
public class VipCardRuleService extends ServiceImpl<VipCardRuleMapper, VipCardRuleAO> implements IVipCardRuleService {
}