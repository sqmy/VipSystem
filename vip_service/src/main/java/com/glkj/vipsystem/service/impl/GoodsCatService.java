package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.entity.ao.GoodsAO;
import com.glkj.vipsystem.entity.ao.GoodsCatAO;
import com.glkj.vipsystem.entity.gen.GoodsPrice;
import com.glkj.vipsystem.entity.gen.Stock;
import com.glkj.vipsystem.mapper.GoodsCatMapper;
import com.glkj.vipsystem.service.IGoodsCatService;
import com.glkj.vipsystem.service.IGoodsPriceService;
import com.glkj.vipsystem.service.IGoodsService;
import com.glkj.vipsystem.service.IStockService;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * GoodsCat 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-04 16:14:44
 */
@Service
public class GoodsCatService extends ServiceImpl<GoodsCatMapper, GoodsCatAO> implements IGoodsCatService {

    @Resource
    private IGoodsService goodsService;
    @Resource
    private IStockService stockService;
    @Resource
    private IGoodsPriceService goodsPriceService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(GoodsCatAO goodsCat) {
        if (goodsCat == null) {
            throw new BizException("商品分类信息错误");
        }
        try {
            this.insert(goodsCat);
        } catch (DuplicateKeyException e) {
            throw new BizException("分类名称重复");
        }
        Integer parentId = goodsCat.getParentId();
        if (parentId == 0) {
            goodsCat.setAllId("" + goodsCat.getId());
        } else {
            GoodsCatAO parent = this.selectById(parentId);
            if (parent == null) {
                throw new BizException("上级分类不存在");
            }
            goodsCat.setAllId(parent.getAllId() + "," + goodsCat.getId());
        }
        this.updateById(goodsCat);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatch(List<Integer> goodsCatIds) {
        if (CollectionUtils.isEmpty(goodsCatIds)) {
            return;
        }
        // 删除商品库存
        for (Integer goodsCatId : goodsCatIds) {
            stockService.delete(new EntityWrapper<Stock>()
                    .where("goods_id in (select id from t_goods where cat_id IN ( SELECT id FROM t_goods_cat WHERE FIND_IN_SET( '" + goodsCatId + "', `all_id` ) ))")
            );
        }
        // 删除商品店铺价格
        for (Integer goodsCatId : goodsCatIds) {
            goodsPriceService.delete(new EntityWrapper<GoodsPrice>()
                    .where("goods_id in (select id from t_goods where cat_id IN ( SELECT id FROM t_goods_cat WHERE FIND_IN_SET( '" + goodsCatId + "', `all_id` ) ))")
            );
        }
        // 删除分类及其子分类下的所有商品
        for (Integer goodsCatId : goodsCatIds) {
            goodsService.delete(new EntityWrapper<GoodsAO>()
                    .where("cat_id IN ( SELECT id FROM t_goods_cat WHERE FIND_IN_SET( '" + goodsCatId + "', `all_id` ) )")
            );
        }
        // 删除分类及其子分类
        for (Integer goodsCatId : goodsCatIds) {
            delete(new EntityWrapper<GoodsCatAO>()
                    .where("FIND_IN_SET('" + goodsCatId + "',`all_id`)")
            );
        }
    }
}
