package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.gen.Stock;

/**
 * <p>
 * 库存 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-06
 */
public interface IStockService extends IService<Stock> {

    Page<Stock> selectShopStocks(Page<Stock> page, Integer companyId, Integer shopId, String goodsName);

    /**
     * 新增库存关联信息
     *
     * @param shopId  店铺id
     * @param goodsId 商品信息
     */
    void addStock(Integer shopId, Integer goodsId);

    Stock selectOne(Integer shopId, Integer depotId, Integer goodsId);

}
