package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.entity.gen.StockApplyFormGoods;
import com.glkj.vipsystem.mapper.StockApplyFormGoodsMapper;
import com.glkj.vipsystem.service.IStockApplyFormGoodsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 出入、移库单-商品关联关系 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
@Service
public class StockApplyFormGoodsServiceImpl extends ServiceImpl<StockApplyFormGoodsMapper, StockApplyFormGoods> implements IStockApplyFormGoodsService {

}
