package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.GoodsAO;
import com.glkj.vipsystem.entity.gen.Goods;

import java.util.List;

/**
 * <p>
 * Goods 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-06 10:18:36
 */
public interface IGoodsService extends IService<GoodsAO> {

    void save(GoodsAO goods);

    void deleteBatch(List<Integer> goodsIds);

}