package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * <p>
 * Company 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-08-28 09:51:08
 */
public interface ICompanyService extends IService<CompanyAO> {

    /**
     * 新增公司
     *
     * @param company 公司信息
     */
    void save(CompanyAO company, Long createUserId);

    /**
     * 修改公司信息
     *
     * @param company 公司信息
     */
    void update(CompanyAO company);

    /**
     * 修改公司密码
     *
     * @param company 公司信息
     */
    void updatePassword(CompanyAO company);

    /**
     * 批量删除
     *
     * @param companyIds 要删除的id
     */
    void deleteBatch(List<Integer> companyIds);

    default CompanyAO selectByUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        return this.selectOne(
                new EntityWrapper<CompanyAO>()
                        .eq("username", username)
                        .last("limit 1")
        );
    }

}
