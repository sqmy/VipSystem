package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.entity.gen.GoodsPrice;
import com.glkj.vipsystem.mapper.GoodsPriceMapper;
import com.glkj.vipsystem.service.IGoodsPriceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品店铺价格 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-03-12
 */
@Service
public class GoodsPriceServiceImpl extends ServiceImpl<GoodsPriceMapper, GoodsPrice> implements IGoodsPriceService {

}
