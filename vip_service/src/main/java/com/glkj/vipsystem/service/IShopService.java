package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.ShopAO;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Shop 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-08-30 15:42:05
 */
public interface IShopService extends IService<ShopAO> {

    void deleteBatch(List<Integer> shopIds);

    /**
     * 删除公司下的店铺
     *
     * @param companyIds 公司id
     */
    default void deleteByCompany(List<Integer> companyIds) {
        if (CollectionUtils.isEmpty(companyIds)) {
            return;
        }
        List<ShopAO> shopAOS = this.selectList(
                new EntityWrapper<ShopAO>()
                        .in("company_id", companyIds)
        );
        if (CollectionUtils.isEmpty(shopAOS)) {
            return;
        }
        List<Integer> shopIds = new ArrayList<>();
        for (ShopAO shopAO : shopAOS) {
            shopIds.add(shopAO.getId());
        }
        this.deleteBatch(shopIds);
    }

    /**
     * 新增店铺
     *
     * @param shop
     */
    void save(ShopAO shop);
}