package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.OrderGoodsAO;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * OrderGoods 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-11 14:56:35
 */
public interface IOrderGoodsService extends IService<OrderGoodsAO> {

    default List<OrderGoodsAO> selectByOrder(Long orderId) {
        if (orderId == null) return new ArrayList<>();
        return selectList(
                new EntityWrapper<OrderGoodsAO>()
                        .eq("order_id", orderId)
        );
    }

}