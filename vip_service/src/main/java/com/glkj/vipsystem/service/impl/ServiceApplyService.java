package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.glkj.vipsystem.entity.ao.ServiceApplyAO;
import com.glkj.vipsystem.mapper.ServiceApplyMapper;
import com.glkj.vipsystem.service.IServiceApplyService;

/**
 * <p>
 * ServiceApply 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-25 11:12:27
 */
@Service
public class ServiceApplyService extends ServiceImpl<ServiceApplyMapper, ServiceApplyAO> implements IServiceApplyService {
}