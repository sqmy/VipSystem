package com.glkj.vipsystem.service;

import com.glkj.vipsystem.entity.gen.StockRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 库存变动记录 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
public interface IStockRecordService extends IService<StockRecord> {

}
