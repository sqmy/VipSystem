package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.VipCardRecordAO;

/**
 * <p>
 * VipCardRecord 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-09 16:43:05
 */
public interface IVipCardRecordService extends IService<VipCardRecordAO> {
}