package com.glkj.vipsystem.service;

import com.glkj.vipsystem.entity.gen.VipCardCancelRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员卡注销记录 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-02-20
 */
public interface IVipCardCancelRecordService extends IService<VipCardCancelRecord> {

}
