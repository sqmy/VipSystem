package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.entity.gen.Notice;
import com.glkj.vipsystem.mapper.NoticeMapper;
import com.glkj.vipsystem.service.INoticeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统通知 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-16
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

}
