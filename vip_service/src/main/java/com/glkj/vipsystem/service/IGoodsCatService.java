package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.GoodsCatAO;

import java.util.List;

/**
 * <p>
 * GoodsCat 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-04 16:14:44
 */
public interface IGoodsCatService extends IService<GoodsCatAO> {

    void save(GoodsCatAO goodsCat);

    void deleteBatch(List<Integer> goodsCatIds);

}