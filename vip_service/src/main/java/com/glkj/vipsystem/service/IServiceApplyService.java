package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.ServiceApplyAO;

/**
 * <p>
 * ServiceApply 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-25 11:12:27
 */
public interface IServiceApplyService extends IService<ServiceApplyAO> {
}