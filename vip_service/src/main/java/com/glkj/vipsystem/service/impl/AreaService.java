package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.glkj.vipsystem.entity.ao.AreaAO;
import com.glkj.vipsystem.mapper.AreaMapper;
import com.glkj.vipsystem.service.IAreaService;

/**
 * <p>
 * Area 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-10-14 10:33:59
 */
@Service
public class AreaService extends ServiceImpl<AreaMapper, AreaAO> implements IAreaService {
}