package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.gen.StockApplyForm;

/**
 * <p>
 * 出入、移库单 服务类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
public interface IStockApplyFormService extends IService<StockApplyForm> {

    /**
     * 新增出入、移库单
     *
     * @param stockApplyForm 出入、移库单信息
     */
    void save(StockApplyForm stockApplyForm, Long actionUserId);

    /**
     * 审核
     */
    void audit(StockApplyForm stockApplyForm, Long verifyUserId);
}
