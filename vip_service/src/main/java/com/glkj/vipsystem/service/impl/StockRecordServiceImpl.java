package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.entity.gen.StockRecord;
import com.glkj.vipsystem.mapper.StockRecordMapper;
import com.glkj.vipsystem.service.IStockRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存变动记录 服务实现类
 * </p>
 *
 * @author LiMuchan
 * @since 2020-01-10
 */
@Service
public class StockRecordServiceImpl extends ServiceImpl<StockRecordMapper, StockRecord> implements IStockRecordService {

}
