package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.glkj.vipsystem.common.exception.BizException;
import com.glkj.vipsystem.entity.ao.CompanyAO;
import com.glkj.vipsystem.entity.ao.EmployeeAO;
import com.glkj.vipsystem.entity.ao.ShopAO;
import com.glkj.vipsystem.entity.gen.Depot;
import com.glkj.vipsystem.entity.gen.GoodsPrice;
import com.glkj.vipsystem.entity.gen.Stock;
import com.glkj.vipsystem.mapper.ShopMapper;
import com.glkj.vipsystem.service.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Shop 服务实现类
 * </p>
 *
 * @author Limuchan
 * @since 2019-08-30 15:42:05
 */
@Service
public class ShopService extends ServiceImpl<ShopMapper, ShopAO> implements IShopService {

    @Resource
    private IEmployeeService employeeService;
    @Resource
    private IStockService stockService;
    @Resource
    private IDepotService depotService;
    @Resource
    private ICompanyService companyService;
    @Resource
    private IGoodsPriceService goodsPriceService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatch(List<Integer> shopIds) {
        if (CollectionUtils.isEmpty(shopIds)) {
            return;
        }
        // 删除店铺员工
        List<EmployeeAO> employeeAOS = employeeService.selectList(
                new EntityWrapper<EmployeeAO>()
                        .in("shop_id", shopIds)
        );
        if (!CollectionUtils.isEmpty(employeeAOS)) {
            List<Integer> employeeIds = new ArrayList<>();
            for (EmployeeAO employeeAO : employeeAOS) {
                employeeIds.add(employeeAO.getId());
            }
            employeeService.deleteBatch(employeeIds);
        }
        // 删除店铺
        this.deleteBatchIds(shopIds);

        // 删除店铺的库存
        depotService.delete(
                new EntityWrapper<Depot>()
                        .in("shop_id", shopIds)
        );
        // 删除库存
        stockService.delete(
                new EntityWrapper<Stock>()
                        .in("shop_id", shopIds)
        );
        // 删除商品店铺价格
        goodsPriceService.delete(
                new EntityWrapper<GoodsPrice>()
                        .in("shop_id", shopIds)
        );
    }

    /**
     * 新增店铺
     *
     * @param shop
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(ShopAO shop) {
        if (shop == null) {
            throw new BizException("店铺信息错误");
        }
        // 校验店铺数量
        Integer companyId = shop.getCompanyId();
        CompanyAO companyAO = companyService.selectById(companyId);
        if (companyAO == null) throw new BizException("公司不存在");
        Integer maxShopNum = companyAO.getMaxShopNum();
        if (maxShopNum > -1) {
            // 不是负数说明有店铺数量限制
            // 计算公司下面的店铺数量
            int shopNum = this.selectCount(
                    new EntityWrapper<ShopAO>()
                            .eq("company_id", companyId)
            );
            if (shopNum >= maxShopNum) {
                throw new BizException("该公司下店铺数量已达上限");
            }
        }
        this.insert(shop);
        depotService.initStock(shop.getId());
    }
}