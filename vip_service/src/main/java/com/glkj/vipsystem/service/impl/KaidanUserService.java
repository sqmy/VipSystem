package com.glkj.vipsystem.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.glkj.vipsystem.entity.gen.KaidanUser;
import com.glkj.vipsystem.mapper.KaidanUserMapper;
import com.glkj.vipsystem.service.IKaidanUserService;
import org.springframework.stereotype.Service;


@Service
public class KaidanUserService extends ServiceImpl<KaidanUserMapper, KaidanUser> implements IKaidanUserService {

}