package com.glkj.vipsystem.service.impl;

import com.glkj.vipsystem.mapper.StatMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * State 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-08-28 09:51:08
 */
@Service
public class StatService {

    @Resource
    private StatMapper statMapper;

    /**
     * 统计销售收入
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    public List<Map> statSale(String startTime, String endTime) {
        return this.statMapper.statSale(startTime, endTime);
    }

    /**
     * 统计订单收入金额
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    public List<Map> statIncomeOrder(String startTime, String endTime) {
        return this.statMapper.statIncomeOrder(startTime, endTime);
    }

    /**
     * 统计会员开卡及充值续费金额
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 数据
     */
    public List<Map> statIncomeVip(String startTime, String endTime) {
        return this.statMapper.statIncomeVip(startTime, endTime);
    }


}
