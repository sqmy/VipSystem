package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.AreaAO;

/**
 * <p>
 * Area 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-10-14 10:33:59
 */
public interface IAreaService extends IService<AreaAO> {
}