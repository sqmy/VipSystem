package com.glkj.vipsystem.service;

import com.baomidou.mybatisplus.service.IService;
import com.glkj.vipsystem.entity.ao.VipCardAO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * VipCard 服务类
 * </p>
 *
 * @author Limuchan
 * @since 2019-09-07 11:38:56
 */
public interface IVipCardService extends IService<VipCardAO> {

    /**
     * 新开会员卡
     *
     * @param vipCard 会员卡信息
     */
    void save(VipCardAO vipCard, String operator);

    /**
     * 批量新增
     *
     * @param vipCardAOS 会员卡信息
     * @param operator   操作员
     */
    void saveBatch(List<VipCardAO> vipCardAOS, String operator);

    /**
     * 批量注销会员卡
     *
     * @param vipCardIds 会员卡id
     */
    void deleteBatch(List<Integer> vipCardIds, String operator);

    /**
     * 调整会员卡
     *
     * @param vipCard 变更的会员卡信息
     */
    void update(VipCardAO vipCard, String operator);

    /**
     * 会员卡消费
     *
     * @param cardId        会员卡id
     * @param shopId        消费店铺id
     * @param balanceAmount 余额消费金额
     * @param costPoints    消费的积分
     * @param givePoints    赠送的积分
     * @param times         消费次数
     * @param operator      收银人员
     */
    void cost(Integer cardId, Integer shopId, BigDecimal balanceAmount, Integer costPoints, Integer givePoints, Integer times, String operator);

}
